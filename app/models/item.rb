class Item < ActiveRecord::Base
  has_many :inventory_items
  has_many :users, :through => :inventory_items
  belongs_to :item_category
  belongs_to :affiliate
  mount_uploader :image, AvatarUploader
  
  validates :affiliate_id, :description, :name, :item_category_id, :presence => true
  
  def self.search(terms)
    if terms
      where(["name LIKE ? or description LIKE ?", "%#{terms}%", "%#{terms}%"])
    else
      all
    end
  end
end
