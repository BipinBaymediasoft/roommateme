class Feedback < ActiveRecord::Base
  belongs_to :feedback_category
  belongs_to :user
    
  validates :content, :presence => true
  
  def user_name
    if self.user_id.present?
      self.user.name
    else
      "Anonymous"
    end
  end
  
  def category_name
    if self.feedback_category_id.present?
      self.feedback_category.name
    else
      "No Category"
    end
  end
end
