class CommunityPartner < ActiveRecord::Base
  belongs_to :community_partner_category
  belongs_to :silo
  mount_uploader :image, AvatarUploader
  
  validates :name, :description, :silo_id, :community_partner_category_id, :presence => true
  
	def self.search(terms, silo)
   if terms
     where(["silo_id = ? and (name LIKE ? or description LIKE ?)", silo.id, "%#{terms}%", "%#{terms}%"])
   else
     all
   end
  end

  def random_cp_from(silo,count=1)
    CommunityPartner.group("id").where("silo_id = ?", silo.id).order("RANDOM()").limit(count)
  end
end