class AdminUser < ActiveRecord::Base
  belongs_to :silo
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, 
         :recoverable, :rememberable, :trackable, :validatable

  validates :email, :presence => true
  validates :name, :presence => true
  validates :silo_id, :presence => true

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :name, :silo_id, :is_super_admin
end
