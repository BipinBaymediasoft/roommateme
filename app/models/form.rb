class Form < ActiveRecord::Base
  has_many :questions
  has_many :answers, :through => :questions
  has_many :responses
  accepts_nested_attributes_for :responses
  
  validates :title, :instructions, :presence => true
  
  # Getter
  def name
    "Survey Version #" + self.id.to_s + " (F" + self.id.to_s + ")"
  end

  # # def collect_questions
  # #   self.questions
  # # end
  # 
  # def count_questions
  #   # collect_questions.count
  #   self.questions.count
  # end

  def collect_users_responses(u)
    # find_all Deprecated
    # Response.find_all_by_user_id_and_form_id(u.id,self.id)
    Response.where(user_id: u.id, form_id: self.id)
  end
  
  # This method seems redundant? Responses are answered questions?
  def collect_users_answered_questions(u)
    #collect_questions.collect!{|q| q.collect_users_answers(u, self)
    self.questions.collect!{ |q| q.collect_users_response(u, self) }
  end
  
  def completed_by_user?(user)
    completed=true
    #collect_answers.each do |q|
    self.questions.each do |q|
      if q.user_has_not_responded?(user,self)
        completed=false
        break
      end
    end
    completed
  end
  
  def started_by_user?(user)
    started=false
    #collect_answers.each do |q|
    self.questions.each do |q|
      if q.user_has_responded?(user,self)
        started=true
        break
      end
    end
    started
  end
  
  def last_answered_question_index(user)
    i = 0
    self.questions.order(:created_at).each do |q|
      if q.user_has_responded?(user,self)
        i += 1
      else
        break
      end
    end
    i
  end
end
