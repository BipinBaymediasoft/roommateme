class Question < ActiveRecord::Base
  has_many :answers
  has_many :responses
  belongs_to :form
  
  validates :content, :form_id, :weight, :presence => true

  # Getter
  def name
    "Question #" + self.id.to_s + " F" + self.form.id.to_s
  end

  def collect_users_response(u,f)
    # Response.find_by_user_id_and_form_id_and_question_id(u.id,f.id,self.id)
    # Updated syntax
    Response.find_by(user_id: u.id, form_id: f.id, question_id: self.id)
  end
  
  def user_has_responses(u,f)
    f.collect_users_responses(u) # Added .length to solve comparison error
  end

  def user_has_not_responded?(u,f)
    # Shouldn't the goal be to return if THIS question was not responded?
    # f.collect_users_responses(u).empty?
    self.collect_users_response(u, f) ? false : true
  end
  
  def user_has_responded?(u,f)
    !user_has_not_responded?(u,f)
  end
end