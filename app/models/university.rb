class University < ActiveRecord::Base
  belongs_to :silo
  has_many :users
  mount_uploader :logo, AvatarUploader
  
  validates :silo_id, :name, :acronym, :presence => true
  validates :domain, :presence =>true, :uniqueness => true
  
end
