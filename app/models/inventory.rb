class Inventory < ActiveRecord::Base
  has_many :items # ?? 
  belongs_to :user # Changed from has_one (both directions?)
  
  validates :user_id, :presence => true
end
