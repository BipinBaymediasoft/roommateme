class InventoryItem < ActiveRecord::Base
  belongs_to :user
  belongs_to :item
  
  validates :inventory_id, :item_id, :user_id, :presence => true
end
