class TutorCategory < ActiveRecord::Base
  has_many :tutors
  
  validates :name, :presence => true
end
