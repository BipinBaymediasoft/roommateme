class SurveyResponse < ActiveRecord::Base
  belongs_to :user
  belongs_to :form
  has_many :responses, dependent: :destroy

  accepts_nested_attributes_for :responses
  validates_associated :responses
  
  validates :user_id, :form_id, :presence => true
    
  def update_responses
    self.user.university.silo.users.each do |u|
      if u.id != self.user.id && u.survey_responses.present? && u.survey_responses.collect {|s| s.form.id}.include?(self.form.id) && (SurveyResponse.find_by_user_id_and_form_id(u.id,self.form.id).responses.count == self.form.questions.count)
        match_points, total_points = 0, 0
        self.form.questions.each do |q|
          total_points += q.weight
          difference = 0
          res1_points = Response.find_by_user_id_and_form_id_and_question_id(u.id,self.form.id,q.id).answer.points.to_i
          res2_points = Response.find_by_user_id_and_form_id_and_question_id(self.user.id,self.form.id,q.id).answer.points.to_i
          difference = (res1_points - res2_points).abs
          if difference == 0
            match_points += q.weight
          elsif difference == 1
            match_points += 0.75 * q.weight
          elsif difference == 2
            match_points += 0.50 * q.weight
          elsif difference == 3
            match_points += 0.25 * q.weight
          elsif difference == 4
            match_points += 0
          else
            match_points += 0
          end
        end
        percentage = ((match_points / total_points.to_f) * 100).to_i
        Match.create({ :user_1_id => self.user.id, :user_2_id => u.id, :percent => percentage })
        Match.create({ :user_2_id => self.user.id, :user_1_id => u.id, :percent => percentage })        
      end
    end
  end
end
