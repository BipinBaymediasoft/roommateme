class FeedbackCategory < ActiveRecord::Base
  scope :all_unless_internal, lambda { 
      where(internal: false)
    }
    
  validates :name, :presence => true
end
