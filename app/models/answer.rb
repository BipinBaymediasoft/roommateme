class Answer < ActiveRecord::Base
  belongs_to :question
  belongs_to :form
  has_many :responses
  
  validates :content, :question_id, :form_id, :points, :presence => true
  
  # Getter
  def name
    "Answer #" + self.id.to_s + " Q" + self.question.id.to_s + " F" + self.question.form.id.to_s
  end

  def is_not_response?(u,f,q)
    Response.find_by_user_id_and_form_id_and_question_id_and_answer_id(u.id, f.id, q.id, self.id).nil?
  end
  
  def is_response?(u,f,q)
    !is_not_response?(u,f,q)
  end
end
