class MessageThread < ActiveRecord::Base
  has_many :messages
  has_many :participancies, :class_name => "Participancy", :foreign_key => "thread_id"
  has_many :participants, :through => :participancies, :source => :user
  
  attr_accessor :to_id
  
  validates :subject, :presence => true
end