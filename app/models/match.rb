class Match < ActiveRecord::Base
  belongs_to :user_1, :class_name => "User", :foreign_key => "user_1_id"
  belongs_to :user_2, :class_name => "User", :foreign_key => "user_2_id"
  
  validates :user_1_id, :user_2_id, :percent, :presence => true
end
