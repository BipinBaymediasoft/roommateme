class User < ActiveRecord::Base
  has_many :responses
  has_many :survey_responses, dependent: :destroy
  has_many :matches, :foreign_key => :user_1_id
  has_many :feedbacks, :dependent => :destroy
  belongs_to :university
  
  has_one :inventory
  has_many :inventory_items
  has_many :wanted_items, -> { where(['want = ?', true]) }, 
            through: :inventory_items, 
            class_name: "Item", 
            source: :item
  has_many :owned_items, -> { where(['have = ?',true]) }, 
            through: :inventory_items, 
            class_name: "Item", 
            source: :item
            
  #friendship relationships
  has_many :friends, -> { where(friendships: {status: "accepted"}) }, through: :friendships
  has_many :requested_friends, -> { where(friendships: {status: "requested"}) }, through: :friendships, source: :friend
  has_many :pending_friends, -> { where(friendships: {status: "pending"}) }, through: :friendships, source: :friend
  has_many :friendships, :dependent => :destroy

  #message relationships
	has_many :participancies, :class_name => "Participancy", :foreign_key => "user_id"
	has_many :message_threads, :through => :participancies
	has_many :messages, :foreign_key => "author_id"
  
  attr_accessible :email, :password, :password_confirmation, :avatar, :latitude, :longitude,
                  :name, :sex, :dob, :survey_id, :flag_count,
                  :needs_roommate, :about_me, :websites, :phone, :perfect_roomate, :activities, :major,
                  :ethnicity, :religion, :music, :shows, :birthtown, :facebook, :aim, :yahoo, :msn, :myspace,
                  :websites, :perfect_roommate, :languages, :willing_to_move
  
  attr_accessor :password
  
  before_save :encrypt_password
	before_create :setup_activation
	after_create :create_inventory
	after_validation :add_to_university, :if => Proc.new { |p| p.email.present? }
	
  #Validations

  validates :email, :presence => true, :uniqueness => true
  #validates :email, :presence => true, :uniqueness => true
  validates_confirmation_of :password
  # In the current state, saves User without password
  validates_presence_of :password, :if => Proc.new { |p| p.password.present? }
  validates :name, :presence => true
  validates :facebook, :format => { :with => /(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:\/~\+#]*[\w\-\@?^=%&amp;\/~\+#])?/, :message => "URL is not a proper URL"}, :if => Proc.new { |p| p.facebook.present? }
  validates :aim, :format => { :with => /(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:\/~\+#]*[\w\-\@?^=%&amp;\/~\+#])?/, :message => "URL is not a proper URL"}, :if => Proc.new { |p| p.aim.present? }
  validates :yahoo, :format => { :with => /(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:\/~\+#]*[\w\-\@?^=%&amp;\/~\+#])?/, :message => "URL is not a proper URL"}, :if => Proc.new { |p| p.yahoo.present? }
  validates :myspace, :format => { :with => /(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:\/~\+#]*[\w\-\@?^=%&amp;\/~\+#])?/, :message => "URL is not a proper URL"}, :if => Proc.new { |p| p.myspace.present? }
  validates :msn, :format => { :with => /(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:\/~\+#]*[\w\-\@?^=%&amp;\/~\+#])?/, :message => "URL is not a proper URL"}, :if => Proc.new { |p| p.msn.present? }
  validates :websites, :format => { :with => /(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:\/~\+#]*[\w\-\@?^=%&amp;\/~\+#])?/, :message => "URL is not a proper URL"}, :if => Proc.new { |p| p.websites.present? }

  mount_uploader :avatar, AvatarUploader
  
  def reset_flag_count!
    self.flag_count = 0
    self.save
  end
  
  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && BCrypt::Password.new(user.password_hash) == password
      user
    else
      nil
    end
  end
  
	def encrypt_password
	  if password.present?
	    self.password_hash = BCrypt::Password.create(password)
	  end
	end
	
	def setup_activation
		self.activated = false
		self.activate_token = SecureRandom.hex(20)
	end
	
	def self.search(terms, silo)
   if terms
     Silo.find(silo.id).users.where(["users.name LIKE ? or email LIKE ? or activities LIKE ? or major LIKE ? or ethnicity LIKE ? or languages LIKE ? or religion LIKE ? or music LIKE ? or shows LIKE ?", "%#{terms}%", "%#{terms}%", "%#{terms}%", "%#{terms}%", "%#{terms}%", "%#{terms}%", "%#{terms}%", "%#{terms}%", "%#{terms}%"])
   else
     all
   end
  end

  def get_match_with(user)
    Match.find_by_user_1_id_and_user_2_id(user.id, self.id)
  end
  
  def random_friend(count=1)
    User.joins(:friends).group("users.id").where("friendships.friend_id = #{self.id}").shuffle[0..count]
  end
  
  def random_match(count=1)
    User.joins(:matches).group("users.id").where("matches.user_2_id = #{self.id}").shuffle[0..count]
  end
  
  def random_inventory(count=1)
    self.owned_items.shuffle[0..count]
  end
  
  private
  
  def create_inventory
    Inventory.create(:user_id => self.id)
  end
  
  def add_to_university
    email_domain = Mail::Address.new(self.email).domain
    domain_name = Domainatrix.parse("http://" + email_domain).domain_with_public_suffix
    university = University.find_by_domain(domain_name.downcase)
    if university.present?
      self.university_id = university.id
    elsif self.errors[:email].blank?
      self.errors.add(:email, "The email you entered is not associated with a university in our system. In order for us to begin the process of including your university in our system, please contact us with the following information: Your name, university email address, university name, university's city and state. Please email us or use the feedback form at the bottom of this page so that we may accommodate you.")
    end
  end
end
