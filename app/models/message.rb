class Message < ActiveRecord::Base
  belongs_to :author, :class_name => "User"
  belongs_to :message_thread # Added 
  validates :body, :message_thread_id, :author_id, :presence => true
end
