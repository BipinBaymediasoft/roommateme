class AffiliateCategory < ActiveRecord::Base
  has_many :affiliates
  
  validates :name, :presence => true
end
