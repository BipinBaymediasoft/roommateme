class Affiliate < ActiveRecord::Base
  belongs_to :affiliate_category
  has_many :items
  mount_uploader :image, AvatarUploader
  
  validates :name, :description, :affiliate_category_id, :presence => true
  
  def self.search(terms)
	 if terms
	   where(["name LIKE ? or description LIKE ?", "%#{terms}%", "%#{terms}%"])
	 else
	   all
	 end
	end
end
