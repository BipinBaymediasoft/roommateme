class Response < ActiveRecord::Base
  belongs_to :user
  belongs_to :form
  belongs_to :question
  belongs_to :answer
  
  validates :user_id, :form_id, :question_id, :answer_id, presence: true
end
