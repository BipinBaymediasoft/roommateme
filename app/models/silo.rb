class Silo < ActiveRecord::Base
  after_save :generate_name
  
  has_many :universities
  has_many :users, :through => :universities
  has_many :admin_users
  has_many :community_partners
  has_many :tutors
  
  validates :city, :state, :presence => true
  
  attr_accessible :city, :state, :name
  
  def generate_name
    if self.name.blank?
      self.update_attributes(:name => self.city + ", " + self.state)
    end
  end
end
