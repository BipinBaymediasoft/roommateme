class Tutor < ActiveRecord::Base
  belongs_to :silo
  belongs_to :tutor_category
  validates :name, :subjects, :contact, :silo_id, :tutor_category_id, :presence => true
  
  mount_uploader :avatar, AvatarUploader
end
