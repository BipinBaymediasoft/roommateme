class CommunityPartnerCategory < ActiveRecord::Base
  has_many :community_partners
  
  validates :name, :presence => true
end
