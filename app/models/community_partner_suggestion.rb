class CommunityPartnerSuggestion < ActiveRecord::Base
  validates :name, :presence => true
  validates :phone, :presence => true
  validates :email, :presence => true
  validates :address, :presence => true
  validates :silo_id, :presence => true
  validates :patronage_frequency, :presence => true
  validates :reason, :presence => true

  belongs_to :silo
end
