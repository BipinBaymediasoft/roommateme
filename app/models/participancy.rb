class Participancy < ActiveRecord::Base
  belongs_to :user
  belongs_to :message_thread, :foreign_key => "thread_id"
  
  validates :thread_id, :user_id, :presence => true
end
