class AffiliateCategoriesController < ApplicationController
  def show
    @categories = AffiliateCategory.all
    @category = AffiliateCategory.find(params[:category])
    @affiliates = @category.affiliates.page(params[:page]).per(10)
  end
end
