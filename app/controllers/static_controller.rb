class StaticController < ApplicationController
  def landing
  end
  
  def subscribe
    if params[:email].blank?
      redirect_to root_url, :notice => "Please enter an email to subscribe."
    else  
      @h = Hominid::API.new('8f716db05733300589f726677236ade5-us2')
      @h.list_subscribe(@h.lists["data"].first["id"], params[:email], {'FNAME' => '', 'LNAME' => ''}, 'html', false, true, true, true)
      redirect_to root_url, :notice => "Signed up! You'll now be notified via email as RoommateMe progresses!"
    end
  end
end