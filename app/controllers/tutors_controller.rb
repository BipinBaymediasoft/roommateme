class TutorsController < ApplicationController
  def index
    @tutors = Tutor.scoped_by_silo_id(current_user.university.silo_id).page(params[:page]).per(10)
    @categories = current_user.university.silo.tutors.map(&:tutor_category_id).uniq.map { |category| TutorCategory.find(category) }
  end

  def show
		@tutor = Tutor.find(params[:id])
  end
end
