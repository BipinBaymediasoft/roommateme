class FeedbacksController < ApplicationController
  def new
    @feedback = Feedback.new
  end

  def create
    @feedback = Feedback.new(feedback_params)    
    if current_user && @feedback.user_id.nil?
      @feedback.user_id = current_user.id
    end
    if @feedback.save
      redirect_to :back, :notice => "Your feedback has been sent to an admin."
    else      
      redirect_to :back, :notice => "Your feedback could not be processed at this time. Try again later."
    end
  end

  def flag
    @user = User.find(params[:id])
    @user.update_attributes(:flag_count => @user.flag_count+1)
    @feedback = Feedback.create(:user_id => current_user.id,
                                :content => "#{current_user.name} (id:#{current_user.id.to_s}) has flagged #{@user.name}'s profile (id:#{@user.id.to_s}).",
                                :feedback_category_id => FeedbackCategory.find_by_name("Flag Report").id)
    redirect_to root_url, :notice => "This user has been flagged for admin inspection."
  end

  private

  def feedback_params
    params.require(:feedback).permit!
  end
end
