class TutorCategoriesController < ApplicationController
  def show
    @categories = current_user.university.silo.tutors.map(&:tutor_category_id).uniq.map { |category| TutorCategory.find(category) }
    @category = TutorCategory.find(params[:category])
    @tutors = @category.tutors.page(params[:page]).per(10)
  end
end
