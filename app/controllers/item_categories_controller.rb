class ItemCategoriesController < ApplicationController
  
  def show
    if params[:category].present?
      @item_category = ItemCategory.find_by_name(params[:category])
      if params[:id].present?
        @affiliate = Affiliate.find(params[:id])
        if @affiliate.present?
          @items = @affiliate.items.where("item_category_id = ?", @item_category.id).order("name desc")
        else
          redirect_to inventory_path, :notice => "Could not find that affiliate."
        end
      else
        @items = @item_category.items.order("name desc")
      end
    else
      redirect_to inventory_path, :notice => "That category does not exist."
    end
  end

end
