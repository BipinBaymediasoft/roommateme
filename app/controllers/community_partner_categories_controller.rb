class CommunityPartnerCategoriesController < ApplicationController
  def show    
    @categories = current_user.university.silo.community_partners.map(&:community_partner_category_id).uniq.map { |category| CommunityPartnerCategory.find(category) }
    @category = CommunityPartnerCategory.find(params[:category])
    @cps = @category.community_partners.page(params[:page]).per(10)
  end
end
