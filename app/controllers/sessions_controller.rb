class SessionsController < ApplicationController
  layout 'session'

  def new
    redirect_to root_path, :notice => "You are already signed in." if current_user
  end

  def create
    user = User.authenticate(session_params[:email], session_params[:password])
    if user
      if user.activated?
        session[:user_id] = user.id
        redirect_to root_url, :notice => "Signed in!"
      else
        flash.now.alert = "Your account isn't activated. Please follow the instructions in the email."
        render "new"        
      end
    else
      flash.now.alert = "Incorrect email or password."
      render "new"
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, :notice => "Signed out!"
  end

  private

  def session_params
    params.require(:session).permit :email, :password
  end
end