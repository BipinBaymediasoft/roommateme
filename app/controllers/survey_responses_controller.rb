class SurveyResponsesController < ApplicationController
  def new
    if current_user.survey_responses.exists?(:form_id => Form.last.id) && (Response.all.where(user_id: current_user.id, form_id: Form.last.id).count == Form.last.questions.count)
      redirect_to matching_path
    end
    if current_user.survey_responses.exists?(form_id: Form.last.id)
      @survey = current_user.survey_responses.last
    else
      @survey = SurveyResponse.new
    end
    @form = Form.last
    @page = ((Response.all.where(user_id: current_user.id, form_id: @form.id).count) * 0.1).to_i
    @questions = []
    for i in (@page*10)...((@page+1)*10)
      @questions << @form.questions.order(:created_at)[i] if i < @form.questions.count
    end
    @last_page = ((@page+1)*10 > @form.questions.count)
    @questions.each do |q|
      @survey.responses.build
    end
  end

  def create
    survey_response_params[:survey_response][:responses_attributes].keys.each do |r|
      survey_response_params[:survey_response][:responses_attributes].delete(r) if survey_response_params[:survey_response][:responses_attributes][r][:answer_id].nil?
    end
    if current_user.survey_responses.exists?(form_id: Form.last.id)
      @survey = current_user.survey_responses.last
      @survey.update_attributes(survey_response_params[:survey_response])
    else
      @survey = SurveyResponse.new(survey_response_params[:survey_response])
      @survey.user = current_user
      @survey.form = Form.last
    end
    if @survey.save!
      if Response.all.where(user_id: current_user.id, form_id: @survey.form.id).count == @survey.form.questions.count
        @survey.update_responses
        redirect_to root_path, :notice => "We will now match you with other users on the site. This may take several minutes."
      elsif Response.all.where(user_id: current_user.id, form_id: @survey.form.id).count > 0 && (Response.all.where(user_id: current_user.id, form_id: @survey.form.id).count % 10 == 0)
        redirect_to take_survey_path
      else
        redirect_to take_survey_path, :notice => "Please fill out this page before proceeding to the next."
      end
    else
      redirect_to take_survey_path, :notice => "Please fill out this page before proceeding to the next."
    end
  end
  
  # def show
  #   if !current_user.survey_responses.blank? && current_user.survey_responses.last.responses.count == Form.last.questions.count
  #     @survey = current_user.survey_responses.last
  #     @questions = @survey.form.questions.order(:created_at)
  #   else
  #     redirect_to take_survey_path, notice: "Please take and finish the survey first."
  #   end
  # end

  private

  def survey_response_params
    params.permit!
  end
end
