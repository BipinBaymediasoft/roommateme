class ApplicationController < ActionController::Base
  protect_from_forgery

	helper_method :current_user
	helper_method :latest_survey
	
	before_filter :feedback
	before_filter :feedback_categories_for_footer

	before_action :mailer_set_url_options
	
	def feedback
	  @feedback ||= Feedback.new
	end
	
	def feedback_categories_for_footer
	  @feedback_categories ||= FeedbackCategory.all_unless_internal.order(:name)
	end
	
	private
	
	def current_user
		@current_user ||= User.find(session[:user_id]) if session[:user_id]
	end

	def mailer_set_url_options
    ActionMailer::Base.default_url_options[:host] = request.host_with_port
  end
end
