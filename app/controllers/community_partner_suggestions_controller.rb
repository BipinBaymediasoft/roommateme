class CommunityPartnerSuggestionsController < InheritedResources::Base
    
    def create
        @community_partner_suggestion = CommunityPartnerSuggestion.new(params[:community_partner_suggestion])
        @community_partner_suggestion.silo = current_user.university.silo
        if @community_partner_suggestion.save then
            flash[:notice] = "Your suggestion was saved successfully. Thank you!"
        else
            flash[:alert] = "Error saving the suggested Community Partner."
            session[:community_partner_suggestion] = @community_partner_suggestion
        end
        #flash.keep
        redirect_to({action: :index, controller: :community_partners})
    end
    
end
