class SignupController < ApplicationController
  layout 'session'
  
	def new
		if current_user
			redirect_to root_url, :notice => "You are already signed up."
		else
			@user = User.new
		end
	end

  def create
		if current_user
			redirect_to root_url, :notice => "You are already signed up." and return
		end

    @user = User.new(user_params)
    if @user.save
      Inventory.create(:user_id => @user.id)
      Notifications.verification(@user.id).deliver
      redirect_to log_in_url, :notice => "Signed up! Please check your email for activation instructions."
    else
      render "new"
    end

  end

  private

  def user_params
    params.require(:user).permit!
  end
end
