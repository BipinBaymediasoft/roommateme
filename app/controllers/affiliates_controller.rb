class AffiliatesController < ApplicationController
  def index
    @affiliates = Affiliate.page(params[:page]).per(10)
    @categories = AffiliateCategory.all
  end
  
	def show
		@affiliate = Affiliate.find(params[:id])
		@item_categories = @affiliate.items.collect { |item| item.item_category }.uniq
	end
end
