class MatchesController < ApplicationController
  def index
    if !current_user.survey_responses.exists?(:form_id => Form.last.id) || (current_user.survey_responses.exists?(form_id: Form.last.id) && (Response.where(user_id: current_user.id, form_id: Form.last.id).count != Form.last.questions.count))
      redirect_to take_survey_path
    else
      @responses = Response.where(user_id: current_user.id)
      @matches = current_user.matches.sort { |a,b| b.percent <=> a.percent }.map { |m| User.find(m.user_2_id) }
    end
  end
end
