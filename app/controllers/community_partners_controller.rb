class CommunityPartnersController < ApplicationController
	def index
		@cps = CommunityPartner.scoped_by_silo_id(current_user.university.silo_id).page(params[:page]).per(10)
    	@categories = current_user.university.silo.community_partners.map(&:community_partner_category_id).uniq.map { |category| CommunityPartnerCategory.find(category) }
        unless session[:community_partner_suggestion] then
	        @community_partner_suggestion = CommunityPartnerSuggestion.new 
        else
	        @community_partner_suggestion = session[:community_partner_suggestion]
	        session[:community_partner_suggestion] = nil
        end
	end
  
	def show
		@cp = CommunityPartner.find(params[:id])
	end
end
