class UniversitiesController < ApplicationController
  def index
    @universities = University.page(params[:page]).per(10)
  end

  def show
  	@university = University.find(params[:id])
  end
end
