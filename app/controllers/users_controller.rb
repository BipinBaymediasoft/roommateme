class UsersController < ApplicationController
	def new
		if current_user
			redirect_to root_url, :notice => "You are already signed up."
		else
			@user = User.new
		end
	end

  def create
		if current_user
			redirect_to root_url, :notice => "You are already signed up." and return
		end

    @user = User.new(user_params)
    if @user.save
      Inventory.create(:user_id => @user.id)
      Notifications.verification(@user.id).deliver
      redirect_to log_in_url, :notice => "Signed up! Please check your email for activation instructions."
    else
      render "new"
    end

  end
  
  def activate
		user = User.find_by_activate_token(params[:token])
		if user
			user.activated = true
			user.save
			redirect_to log_in_path, :notice => "Account activated! Sign in to begin."
		else
			redirect_to log_in_path, :notice => "Incorrect activation link. Please check your email and try again."
		end
	end
	
	def edit
	  @user = current_user
  end
	
	def update
    @user = User.find(params[:id])
    if @user.update_attributes(params[:user])
      @user.encrypt_password
      redirect_to @user, :notice => 'Your profile was successfully updated.'
    else
      render "edit"
    end
	end

	def show
	  if current_user
			@user = User.find(params[:id] || current_user)
			@friends = @user.friends.shuffle[0..2]
			@matches = @user.random_match(3)
			@inventoryitems = @user.random_inventory(3)
			if params[:id].present? && (params[:id].to_i == current_user.id)
			  @isDashboard = true
			elsif params[:id].blank? && current_user
			  @isDashboard = true
		  else
		    @isDashboard = false
			end
			if @isDashboard
			 @new_friend_requests = Friendship.where("status = ? and friend_id = ?","requested",current_user.id).count
			 @new_messages = 0
			 current_user.message_threads.where("is_read = ?", false).each do |t|
			   @new_messages += 1 if t.messages.last.author_id != current_user.id
		   end
			end
		else
			redirect_to log_in_url, :notice => "You must be logged in to view profiles."
		end
	end
	
  def recovery
  end
  
  def send_recovery
    user = User.find_by_email(params[:email])
    if user.present?
      user.recovery = SecureRandom.hex(20)
      user.save
      Notifications.recovery(user.id).deliver
      redirect_to log_in_path, :notice => "Password reset instructions have been sent to your email address."
    else
      redirect_to recovery_path, :notice => "Incorrect email."
    end
  end
  
  def reset_form
    @user = User.find_by_recovery(params[:token])
    if @user.nil?
      redirect_to recovery_path, :notice => "Please request a new password recovery link."
    end
  end
  
  def reset_password
    @user = User.find_by_recovery(params[:user][:recovery])
    if params[:user][:password].blank?
      @user.errors.add(:password, "can't be blank")
      render "reset_form"
    elsif @user.update_attributes(params[:user])
      @user.recovery = nil
      save_bool = @user.save
      redirect_to log_in_path, :notice => "Your password has been reset."
    else
      render "reset_form"
    end
  end
  
  def messages
    @threads = current_user.message_threads.page(params[:page]).per(10).order("updated_at desc")
  end
  
  def items
    @item_categories = ItemCategory.all
  end
  
  def have_item
    if current_user.owned_items.exists?(params[:item_id])
      item = current_user.inventory_items.find(:item_id => params[:item_id])
      item.have = true
      item.save
    else
      item = current_user.inventory_items.build(:item_id => params[:item_id], :have => true, :inventory_id => current_user.inventory.id)
      item.save
    end
    # respond_to do |format|
    #   format.js   { render :nothing => true }
    # end
    redirect_to :back
  end
  
  def want_item
    if current_user.wanted_items.exists?(params[:item_id])
      item = current_user.inventory_items.find(:item_id => params[:item_id])
      item.want = true
      item.save
    else
      item = current_user.inventory_items.build(:item_id => params[:item_id], :want => true, :inventory_id => current_user.inventory.id)
      item.save
    end
    # respond_to do |format|
    #   format.js   { render :nothing => true }
    # end
    redirect_to :back    
  end
  
  def delete_have_item
    if current_user.inventory_items.exists?(:item_id => params[:item_id], :have => true)
      item = current_user.inventory_items.find_by_item_id_and_have(params[:item_id], true)
      item.destroy
    end
    # respond_to do |format|
    #   format.js   { render :nothing => true }
    # end
    redirect_to :back
  end
  
  def delete_want_item
    if current_user.inventory_items.exists?(:item_id => params[:item_id], :want => true)
      item = current_user.inventory_items.find_by_item_id_and_want(params[:item_id], true)
      item.destroy
    end
    # respond_to do |format|
    #   format.js   { render :nothing => true }
    # end
    redirect_to :back
  end

  private

  def user_params
    params.require(:user).permit!
  end
end
