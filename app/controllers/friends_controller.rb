class FriendsController < ApplicationController  
  def index
    @user = User.find(params[:user_id])
    @friends = @user.friends.page(params[:friends_page]).per(10)
    @pending_friends = @user.pending_friends.page(params[:pending_page]).per(10)
    @requested_friends = @user.requested_friends.page(params[:requested_page]).per(10)
  end
  
  def show
    redirect_to user_path(params[:id])
  end
  
  def new
    @friendship1 = Friendship.new
    @friendship2 = Friendship.new
  end
  def create
    @user = User.find(current_user)
    @friend = User.find(friend_params[:friend_id])
    friend_params[:friendship1] = {:user_id => @user.id, :friend_id => @friend.id, :status => 'requested'}
    friend_params[:friendship2] = {:user_id => @friend.id, :friend_id => @user.id, :status => 'pending'}
    @friendship1 = Friendship.create(friend_params[:friendship1])
    @friendship2 = Friendship.create(friend_params[:friendship2])
    if @friendship1.save && @friendship2.save
      redirect_to user_friends_path(current_user)
    else
      redirect_to user_path(current_user)
    end
  end
  def update
    @user = User.find(current_user)
    @friend = User.find(params[:id])
    params[:friendship1] = {:user_id => @user.id, :friend_id => @friend.id, :status => 'accepted'}
    params[:friendship2] = {:user_id => @friend.id, :friend_id => @user.id, :status => 'accepted'}
    @friendship1 = Friendship.find_by_user_id_and_friend_id(@user.id, @friend.id)
    @friendship2 = Friendship.find_by_user_id_and_friend_id(@friend.id, @user.id)
    if @friendship1.update_attributes(friend_params[:friendship1]) && @friendship2.update_attributes(friend_params[:friendship2])
      flash[:notice] = 'Friend sucessfully accepted!'
      redirect_to user_friends_path(current_user)
    else
      redirect_to user_path(current_user)
    end
  end
  def destroy
    @user = User.find(params[:user_id])
    @friend = User.find(params[:id])
    @friendship1 = @user.friendships.find_by_friend_id(params[:id]).destroy
    @friendship2 = @friend.friendships.find_by_friend_id(params[:user_id]).destroy
    redirect_to user_friends_path(:user_id => current_user)
  end

  def friend_params
    params.permit!
  end
end
