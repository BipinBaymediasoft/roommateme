class MessagesController < ApplicationController
  def index
    @threads = current_user.message_threads.page(params[:page]).per(10).order("updated_at desc")
  end
  
  def new
    @message = Message.new
    @thread = MessageThread.new
    @user = User.find(params[:to_id]) if params[:to_id].present?
  end
  
	def create
	  if current_user.id != params[:to_id]  
  	  @message = Message.new
      @thread = MessageThread.new
      @message.update_attributes(message_params[:message])
      @thread.update_attributes(message_params[:message_thread])
      @thread.messages << @message
      current_user.messages << @message
      @thread.participancies.build(:user_id => current_user.id)
      @thread.participancies.build(:user_id => message_params[:message_thread][:to_id])
      if @thread.save && @message.save
        redirect_to messages_path, :notice => "Sent message."
      else  
        redirect_to messages_path, :notice => "Unable to send message."  
      end
    else
      redirect_to messages_path, :notice => "You can't send yourself a message."  
    end
  end 
  
  def show
    @thread = MessageThread.find(params[:id])
    @response = Message.new
    if !@thread.is_read? && @thread.messages.last.author.id != current_user.id
      @thread.is_read = true
      @thread.save!
    end

    @author = @thread.messages.first.author
  end
  
  def mark_as_read
    @thread = MessageThread.find(params[:id])
    @thread.is_read = true
    @thread.save!
    redirect_to messages_path
  end
  
  def mark_as_unread
    @thread = MessageThread.find(params[:id])
    @thread.is_read = false
    @thread.save!
    redirect_to messages_path
  end
  
  def respond
    @response = Message.new
    @thread = MessageThread.find(params[:id])
    @response.body = params[:message][:body]
    @thread.messages << @response
    current_user.messages << @response
    @thread.is_read = false
    @response.save

    @thread = MessageThread.find(params[:id])
    @response = Message.new

    @author = @thread.messages.first.author

    render :show
  end

  private

  def message_params
    params.permit!
  end
end
