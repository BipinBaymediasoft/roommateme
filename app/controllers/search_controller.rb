class SearchController < ApplicationController
  def search
    unless params[:q] == "" or params[:q].nil?
      @users = User.search(params[:q], current_user.university.silo)
      @users_slice = @users.slice(0,10)
      @community_partners = CommunityPartner.search(params[:q], current_user.university.silo)
      @cp_slice = @community_partners.slice(0,10)
      @affiliates = Affiliate.search(params[:q])
      @aff_slice = @affiliates.slice(0,10)
      @items = Item.search(params[:q])
      @items_slice = @items.slice(0,10)
      @total_count = @users.count + @community_partners.count + @affiliates.count + @items.count
    end
  end
end
