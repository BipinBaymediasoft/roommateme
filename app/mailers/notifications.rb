class Notifications < ActionMailer::Base
  default :from => "support@roommateme.net"

  def recovery(user_id)
    @user = User.find(user_id)

    mail :to => @user.email, :subject => "[RoommateMe] Password reset instructions" 
  end

  def verification(user_id)
    @user = User.find(user_id)

    mail :to => @user.email, :subject => "[RoommateMe] Account activation instructions" 
  end
end
