ActiveAdmin.register Match do
  permit_params :user_1_id, :user_2_id, :percent
end
