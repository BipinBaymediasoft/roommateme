ActiveAdmin.register Tutor do
  permit_params :name, :avatar, :subjects, :tutor_category_id, :contact, :silo_id
  menu :parent => "Community"
  
  index do
    column :id
    column :image do |tutor|
      image_tag tutor.avatar_url(:thumb)
    end
    column :name
    column "Category", :tutor_category, :sortable => :tutor_category_id do |tutor|
       tutor.tutor_category.name
    end
    actions
  end
  filter :tutor_category, :as => :select, :collection => lambda{ TutorCategory.all }

  form :html => { :multipart => true } do |f|
      f.inputs "Details" do
        f.input :name
        f.input :avatar, :as => :file
        f.input :subjects
        f.input :tutor_category_id, :as => :select, :collection => TutorCategory.order("name desc")
        f.input :contact
        f.input :silo_id, :as => :select, :collection => Silo.order("state desc").order("city desc")
      end
      f.actions
  end
end
