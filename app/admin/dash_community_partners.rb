ActiveAdmin.register CommunityPartner do
  permit_params :name, :image, :address, :google_maps, :community_partner_category_id, :silo_id, :description
  menu :parent => "Community"

  index do
    column :id
    column :image do |partner|
      image_tag partner.image_url(:thumb)
    end
    column :name
    column "Category", :community_partner_category, :sortable => :community_partner_category_id do |partner|
       partner.community_partner_category.name
     end
    column :silo, :sortable => :silo_id do |partner|
      link_to partner.silo.city + ", " + partner.silo.state, admin_silo_path(partner.silo)
    end
    column "" do |partner|
      if current_admin_user.silo.community_partners.all.include?(partner) || current_admin_user.is_super_admin
        links = link_to "View", resource_path(partner), :class => "member_link view_link"
        links += link_to "Edit", edit_resource_path(partner), :class => "member_link edit_link"
        links += link_to "Delete", resource_path(partner), :method => :delete, :confirm => ("Are you sure you want to delete this Community Affiliate?"), :class => "member_link delete_link"
        links
      end
    end
  end
  filter :silo, :as => :select, :collection => lambda{ Silo.all }
  filter :community_partner_category, :as => :select, :collection => lambda{ CommunityPartnerCategory.all }

  form :html => { :multipart => true } do |f|
      f.inputs "Details" do
        f.input :name
        f.input :image, :as => :file 
        f.input :address
        f.input :google_maps
        f.input :community_partner_category_id, :as => :select, :collection => CommunityPartnerCategory.order("name desc")
        f.input :silo_id, :as => :select, :collection => Silo.order("state desc").order("city desc")
        f.input :description
      end
      f.actions
  end
end