ActiveAdmin.register Question do
  permit_params :form_id, :content, :weight

  menu :parent => "Survey"
  
  index do
    column :id
    column "Content" do |question|
      link_to question.content, admin_question_path(question)
    end
    column "Belongs To" do |question|
      link_to question.form.name, admin_form_path(question.form)
    end
    column :weight
  end
  filter :form, :as => :select, :collection => lambda{ Form.all }
end
