ActiveAdmin.register CommunityPartnerCategory do
  permit_params :name
  
  menu :parent => "Community" 
end
