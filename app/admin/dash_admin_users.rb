ActiveAdmin.register AdminUser do
  permit_params :email, :password, :password_confirmation, :remember_me, :name, :silo_id, :is_super_admin

  index do
    column :id
    column :name
    column :email
    column :is_super_admin if current_admin_user.is_super_admin
    column :silo, :sortable => :silo_id do |admin_user|
      link_to admin_user.silo.city + ", " + admin_user.silo.state, admin_silo_path(admin_user.silo)
    end
    column "" do |admin_user|
      if current_admin_user.is_super_admin | current_admin_user==admin_user
        links = link_to "View", resource_path(admin_user), :class => "member_link view_link"
        links += link_to "Edit", edit_resource_path(admin_user), :class => "member_link edit_link"
        links += link_to "Delete", resource_path(admin_user), :method => :delete, :confirm => ("Are you sure you want to delete this Admin?"), :class => "member_link delete_link"
        links
      end
    end
    actions
  end
  filter :silo, :as => :select, :collection => lambda{ Silo.all }
  
  form do |f|
    f.inputs "Details" do
      f.input :name
      f.input :email
      f.input :silo_id, :as => :select, :collection => Silo.order("state desc").order("city desc")
      f.input :password
      f.input :is_super_admin
    end
    f.actions
  end
end