ActiveAdmin.register AffiliateCategory do
  permit_params :name

  menu :parent => "Community"
end
