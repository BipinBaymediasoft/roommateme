ActiveAdmin.register User do
  index do
    column :id
    column :avatar do |user|
      image_tag user.avatar_url(:thumb)
    end
    column :name
    column :email
    column :address
    column :city
    column :state
    column :country
    column :zipcode
    column :flag_count
    column :university, :sortable => :university_id do |user|
      	if (user.university) then 
		link_to user.university.name, admin_university_path(user.university) 
	end
    end
    column :silo do |user|
      	if (user.university) then  
		link_to user.university.silo.city + ", " + user.university.silo.state, admin_silo_path(user.university.silo) if user.university.present? 
	end
    end
    column "" do |user|
      if current_admin_user.silo.universities.all.include?(user.university) || current_admin_user.is_super_admin
        links = link_to "View", resource_path(user), :class => "member_link view_link"
        links += link_to "Edit", edit_resource_path(user), :class => "member_link edit_link"
        links += link_to "Reset Flag Count", reset_flag_count_admin_user_path(user), :class => "member_link edit_link"
        links += link_to "Delete", resource_path(user), :method => :delete, :confirm => ("Are you sure you want to delete this User?"), :class => "member_link delete_link"
        links
      end
    end
  end
  filter :university, :as => :select, :collection => lambda{ University.all }
  scope "Flagged" do |users|
    users.where("flag_count >= ?",5)
  end
  
  member_action :reset_flag_count do
    User.find(params[:id]).reset_flag_count!
    redirect_to(:back)
  end

  form do |f|
    f.inputs "Details" do
      f.input :name
      f.input :avatar, :as => :file
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end
end
