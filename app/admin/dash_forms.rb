ActiveAdmin.register Form do
  permit_params :title, :instructions
  
  menu :parent => "Survey"
end
