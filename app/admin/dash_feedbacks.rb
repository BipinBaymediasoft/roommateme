ActiveAdmin.register Feedback, :as => "review" do
  permit_params :feedback_category_id, :user_id, :content

  index do
    column :id
    column "Submitted By", :user_name, :sortable => :user_id do |feedback|
      link_to_if feedback.user_id.present?, feedback.user_name, admin_user_path(feedback.user_id || 0)
    end
    column "Category", :category_name, :sortable => :feedback_category_id
    column :content
    column :university, :sortable => :university_id do |feedback|
      if feedback.user_id.present?
        link_to feedback.user.university.name, admin_university_path(feedback.user.university) if feedback.user.university.present?
      else
        "N/A"
      end
    end
    actions
  end
  filter :user, :as => :select, :collection => lambda{ User.all }
  filter :feedback_category, :as => :select, :collection => lambda{ FeedbackCategory.all }
end
