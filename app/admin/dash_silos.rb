ActiveAdmin.register Silo do
  index do
    column :id
    column :name
    column :city
    column :state
    column :universities do |silo|
      silo.universities.count.to_s
    end
    column :users do |silo|
      users=0
      silo.universities.each do |uni|
        users+=uni.users.count
      end
      users.to_s
    end
    column "" do |silo|
      if current_admin_user.silo.id==silo.id || current_admin_user.is_super_admin
        links = link_to "View", resource_path(silo), :class => "member_link view_link"
        links += link_to "Edit", edit_resource_path(silo), :class => "member_link edit_link"
        links += link_to "Delete", resource_path(silo), :method => :delete, :confirm => ("Are you sure you want to delete this Silo?"), :class => "member_link delete_link"
        links
      end
    end
  end

  form :html => { :multipart => true } do |f|
      f.inputs "Details" do |silo|
        f.input :city
        f.input :state
        f.input :name, :label => "Name (optional)"
      end
      f.button
  end
end