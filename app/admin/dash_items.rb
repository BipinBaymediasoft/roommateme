ActiveAdmin.register Item do
  permit_params :item_category_id, :affiliate_id, :description, :image, :name, :html_link
  menu :parent => "Inventory"
  
  index do
    column :image do |item|
      image_tag item.image_url(:thumb)
    end
    column :id
    column :description
    column :affiliate_id do |item|
      item.affiliate.name
    end
    column :item_category_id do |item|
      item.item_category.name
    end
    actions
  end
  filter :affiliate_id, :as => :select, :collection => lambda{ Affiliate.all }
  filter :item_category_id, :as => :select, :collection => lambda{ ItemCategory.all }
  
  form :html => { :multipart => true } do |f|
      f.inputs "Details" do
        f.input :item_category_id, :as => :select, :collection => ItemCategory.order("name")
        f.input :affiliate_id, :as => :select, :collection => Affiliate.order("name")
        f.input :description
        f.input :image, :as => :file
        f.input :name
        f.input :html_link
      end
      f.actions
  end
end
