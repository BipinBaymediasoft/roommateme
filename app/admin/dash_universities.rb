ActiveAdmin.register University do
  permit_params :name, :logo, :acronym, :domain, :address, :google_maps, :silo_id
  index do
    column :id
    column :logo do |university|
      image_tag university.logo_url(:thumb)
    end
    column :name
    column :acronym
    column :users do |university|
      university.users.count.to_s
    end
    column :silo, :sortable => :silo_id do |university|
      link_to university.silo.city + ", " + university.silo.state, admin_silo_path(university.silo)
    end
    column "" do |university|
      if current_admin_user.silo.universities.all.include?(university) || current_admin_user.is_super_admin
        links = link_to "View", resource_path(university), :class => "member_link view_link"
        links += link_to "Edit", edit_resource_path(university), :class => "member_link edit_link"
        links += link_to "Delete", resource_path(university), :method => :delete, :confirm => ("Are you sure you want to delete this University?"), :class => "member_link delete_link"
        links
      end
    end
  end
  filter :silo, :as => :select, :collection => lambda{ Silo.all }

  form :html => { :multipart => true } do |f|
      f.inputs "Details" do
        f.input :name
        f.input :logo, :as => :file
        f.input :acronym
        f.input :domain
        f.input :address
        f.input :google_maps
        f.input :silo_id, :as => :select, :collection => Silo.order("state desc").order("city desc")
      end
      f.actions
  end
end