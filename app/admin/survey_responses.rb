ActiveAdmin.register SurveyResponse do
  permit_params :user_id, :form_id
end
