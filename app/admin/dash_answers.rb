ActiveAdmin.register Answer do
  permit_params :question_id, :form_id, :content, :points
  menu :parent => "Survey"
  
  index do
    column :id
    column "Content" do |answer|
      link_to answer.content, admin_answer_path(answer)
    end
    column :points    
    column "Answer To" do |answer|
      link_to answer.question.name, admin_question_path(answer.question)
    end
    column "Belongs To" do |answer|
      link_to answer.form.name, admin_form_path(answer.question.form)
    end
  end
  filter :question, :as => :select, :collection => lambda{ Question.all }
end
