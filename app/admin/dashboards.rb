ActiveAdmin::register_page "Dashboard" do
  
  content do
    columns do

      column do
        panel "Site Numbers" do
          table do
            tr do
              th "Feature"
              th "Size"
            end
            tr do
              td link_to "Silos", admin_silos_path
              td Silo.count
            end
            tr do
              td link_to "Universities", admin_universities_path
              td University.count
            end
            tr do
              td link_to "Affiliates", admin_affiliates_path
              td Affiliate.count
            end
            tr do
              td link_to "Community Partners", admin_community_partners_path
              td CommunityPartner.count
            end
            tr do
              td link_to "Users", admin_users_path
              td User.count
            end
          end
        end
        panel "Recent Users" do
          table_for User.order('created_at desc').limit(10) do
            column("Email")       { |user| mail_to user.email, user.email } 
            column("University")  { |user| if (user.university_id) then link_to user.university.name, user.university end} 
            column("Silo")        { |user| if (user.university_id) then link_to user.university.silo.name, admin_silo_path(user.university.silo) end} 
          end
        end
      end

      column do
        panel "Recent Reviews" do
          table_for Feedback.order('created_at desc').limit(10) do
            column("Category")      { |feedback| status_tag(feedback.category_name) } 
            column("Submitted By")  { |feedback| link_to_if feedback.user_id.present?, feedback.user_name, admin_user_path(feedback.user_id || 0) } 
            column("View")          { |feedback| link_to "View", admin_review_path(feedback) }
          end
        end
      end

      column do
        panel "Averages" do

          if Message.count > 0
            messages_per_silo = Message.includes(:author => { :university => :silo }).group_by {|u| u.author.university.silo_id }.length / Message.count
            messages_per_user = Message.all.group_by {|a| a.author_id }.length / Message.count
          else
            messages_per_silo = 0
            messages_per_user = 0
          end

          span "Silo Averages"
          ul do
            li "Universities per silo: #{University.count.try(:/, Silo.count.nonzero? || 1) || 0}"
            li "Community Partners per silo: #{CommunityPartner.count.try(:/, Silo.count.nonzero? || 1) || 0}"
            li "Users per silo: #{User.count.try(:/, Silo.count.nonzero? || 1) || 0}"
            li "Compatibility per silo: #{Match.includes(:user_1 => { :university => :silo}, :user_2 => { :university => :silo }).group("universities.silo_id").average(:percent).collect { |hash| hash.second.to_i }.inject(:+).try(:/, Silo.count) || 0}"
            li "Number of messages sent per silo: #{messages_per_silo}"
          end

          span "University Averages"
          ul do
            li "Community Partners per silo: #{CommunityPartner.count / (University.count.nonzero? || 1)}"
            li "Users per silo: #{User.count / (University.count.nonzero? || 1)}"
          end

          span "User Averages"
          ul do
            li "Number of messages sent per user: #{messages_per_user}"
          end
        end
      end

    end
  end
end
