ActiveAdmin.register Affiliate do
  permit_params :name, :image, :affiliate_category_id, :html_link, :description 

  menu :parent => "Community"

  index do
    column :id
    column :image do |affiliate|
      image_tag affiliate.image_url(:thumb)
    end
    column :name
    column "Category", :affiliate_category, :sortable => :affiliate_category_id do |affiliate|
       affiliate.affiliate_category.name
    end
    actions
  end
  filter :affiliate_category, :as => :select, :collection => lambda{ AffiliateCategory.all }

  form :html => { :multipart => true } do |f|
      f.inputs "Details" do
        f.input :name
        f.input :image, :as => :file 
        f.input :affiliate_category_id, :as => :select, :collection => AffiliateCategory.order("name desc")
        f.input :html_link, :as => :text
        f.input :description, :as => :text
      end
      f.actions
  end
end