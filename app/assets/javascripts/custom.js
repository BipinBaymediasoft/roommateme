jQuery(document).ready(function() {
  var age_slider = document.getElementById("age_range");
  var budget_slider = document.getElementById("budget_range");

  if (age_slider && budget_slider) {
    var age_output = document.getElementById("age_value");
    var budget_output = document.getElementById("budget_value");
    age_output.innerHTML = age_slider.value;
    budget_output.innerHTML = budget_slider.value;

    age_slider.oninput = function() {
      age_output.innerHTML = this.value;
    }

    budget_slider.oninput = function() {
      budget_output.innerHTML = this.value;
    }
  }

  $('.friends-slider .slider').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    dots: false,
    infinite: true,
    arrows: true,
    prevArrow: '<div class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
    nextArrow: '<div class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>',
    autoplay: true,
    autoplaySpeed: 2000,
    // fade: true,
    cssEase: 'linear',

    responsive: [{
        breakpoint: 1500,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true
        }
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true
        }
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });


  $('.slider').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    dots: false,
    infinite: true,
    arrows: true,
    prevArrow: '<div class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
    nextArrow: '<div class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>',
    autoplay: true,
    autoplaySpeed: 2000,
    // fade: true,
    cssEase: 'linear',

    responsive: [{
        breakpoint: 1500,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true
        }
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true
        }
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });

  var autocomplete;

  // Create the autocomplete object, restricting the search predictions to
    // geographical location types.
  autocomplete = new google.maps.places.Autocomplete(
      document.getElementById('user_address'), {types: ['geocode']});

  // Avoid paying for data that you don't need by restricting the set of
  // place fields that are returned to just the address components.
  autocomplete.setFields(['address_component']);

  $('#user_address').keydown(function (e) {
    if (e.keyCode == 13) {
      return false;
    }
  });

  // When the user selects an address from the drop-down, populate the
  // address fields in the form.
  autocomplete.addListener('place_changed', fillInAddress);
});

function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  document.getElementById("user_city").value = '';
  document.getElementById("user_state").value = '';
  document.getElementById("user_zipcode").value = '';
  document.getElementById("user_country").value = '';

  // Get each component of the address from the place details,
  // and then fill-in the corresponding field on the form.

  console.log(place.address_components);

  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (addressType == 'locality') {
      var val = place.address_components[i]["long_name"];
      document.getElementById('user_city').value = val;
    }

    if (addressType == 'administrative_area_level_1') {
      var val = place.address_components[i]["long_name"];
      document.getElementById('user_state').value = val;
    }

    if (addressType == 'country') {
      var val = place.address_components[i]["long_name"];
      document.getElementById('user_country').value = val;
    }

    if (addressType == 'postal_code') {
      var val = place.address_components[i]["short_name"];
      document.getElementById("user_zipcode").value = val;
    }
  }
}