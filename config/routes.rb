Roommateme::Application.routes.draw do  # 
	# Logged in
  # 
  constraints lambda { |req| !req.session[:user_id].blank? } do
    resources :users do
      resources :friends
    end    
    
    # Match route
    get 'matches' => "matches#index", :as => "matching"
    
    # Survey routes
    scope 'survey' do
      get '/' => "survey_responses#new", as: "take_survey"
      post 'submit' => "survey_responses#create", :as => "submit_survey"
      # get '/answers' => "survey_responses#show", :as => "view_survey"
    end
    
    # Messaging routes
    resources :messages, :only => [:new, :create, :show, :index] do
      get "/mark_as_read" => "messages#mark_as_read", :on => :member
      get "/mark_as_unread" => "messages#mark_as_unread", :on => :member
      get "/to/:to_id" => "messages#new", :as => "new_message", :on => :collection
		  post "/to/:to_id/create" => "messages#create", :as => "create_message", :on => :collection
		  post "/respond" => "messages#respond", :as => "respond_message", :on => :member
    end
	  
    # Search routes
    get "/search" => "search#search", :as => "search"
	  
    # Inventory routes
	  scope "/inventory" do
	    get '/' => "users#items", :as => :inventory
      get "/have/:item_id" => "users#have_item", :as => "inventory_have"
      get "/want/:item_id" => "users#want_item", :as => "inventory_want"
      delete "/have/:item_id" => "users#delete_have_item", :as => "inventory_have_delete"
      delete "/want/:item_id" => "users#delete_want_item", :as => "inventory_want_delete"
      get "/:category" => "item_categories#show", :as => :item_categories
    end
    
    # Community routes
    get "/community" => "community#index", :as => :community
    resources :affiliates, :only => [:index, :show] do
      get "/category/:category" => "affiliate_categories#show", :on => :collection, :as => :category
    end
    resources :community_partners, :only => [:index, :show] do
      get "/category/:category" => "community_partner_categories#show", :on => :collection, :as => :category
    end
    resources :tutors, :only => [:index, :show] do
      get "/category/:category" => "tutor_categories#show", :on => :collection, :as => :category
    end
    
    # Root route
	  root :to => "users#show", :as => "dashboard"
  end
  
  # browse route
  get 'browse' => "browse#index", :as => "browse"

  # 
  # Not logged in
  # 
  resources :universities, :only => [:index, :show]
  post "/users" => "users#create"
  
  # Session routes
  resources :sessions, :only => [:new, :create, :destroy]
  get "sign_out" => "sessions#destroy", :as => "log_out"
  get "sign_in" => "sessions#new", :as => "log_in"
  
  # Static routes
  get "about" => "static#about", :as => "about"
  get "terms" => "static#tos", :as => "terms"
  
  # Password reset and recovery routes
  # get "register" => "users#new", :as => "register"
  get "register" => "signup#new", :as => "register"
  post "register" => "signup#create", :as => "signup"
	get "recovery" => "users#recovery", :as => "recovery"
	post "send_recovery" => "users#send_recovery", :as => "send_recovery"
	get "reset/:token" => "users#reset_form", :as => "reset_form"
	put "reset" => "users#reset_password", :as => "reset_password"
	get "activate/:token" => "users#activate", :as => "activate"
	
  # Newsletter subscribe path
  post "subscribe" => "static#subscribe", :as => "subscribe"
  
  # Feedback routes
  post "feedback" => "feedbacks#create", :as => "feedbacks"
  get "flag/:id" => "feedbacks#flag", :as => "flag_user"
    
  # Admin routes
  ActiveAdmin.routes(self)
  devise_for :admin_users, ActiveAdmin::Devise.config
  
  # Root route
  root :to => "static#landing"
  resources :community_partner_suggestions
  
  # Catch-all route
  # If you're not logged in, then none of the routes in the constraints block
  # will be defined (Rails 404s), so if you're trying to access one of those pages,
  # this will pick up and redirect you to sign in instead of just throwing a 404.
  match '*path' => redirect('/sign_in'), via: [:get, :post], :constraints => lambda { |req| req.session[:user_id].blank? }
end
