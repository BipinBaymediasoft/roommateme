# Add RVM's lib directory to the load path.
# $:.unshift(File.expand_path('./lib', ENV['rvm_path']))
# Load RVM's capistrano plugin.
require "rvm/capistrano"
# Set it to the ruby + gemset of your app, e.g:
#set :rvm_ruby_string, 'ruby-2.1.2'
set :rvm_ruby_string, 'ruby-1.9.2-p180'

set :stages, %w(staging production)
set :default_stage, "staging"

require 'bundler/capistrano'
require 'capistrano/ext/multistage'

set :application, "roommateme"
#set :repository,  "ssh://maziar@roommateme.net:/repo/roommateme.git"
set :repository,  "git@bitbucket.org:maziara/roommateme.git"
set :branch, "master"
set :deploy_via, :remote_cache

set :scm, :git
set :scm_username, "newaperio"

set :config_files, %w( database.yml )

role :web, "roommateme.net"                          # Your HTTP server, Apache/etc
role :app, "roommateme.net"                          # This may be the same as your `Web` server
role :db,  "roommateme.net", :primary => true # This is where Rails migrations will run
set :deploy_to, "/var/www/roommateme"
set :use_sudo, false
set :user, 'root'

set :config_files, %w( database.yml )

namespace :deploy do
  task :start do ; end
  task :stop do ; end
  
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end
  
  task :make_public do
    run "chmod -R 777 #{release_path}/public/"
  end
  
  task :database_yml do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"     
  end  
end

# ==========================================================================
# Uploads
# via http://www.simonecarletti.com/blog/2009/02/capistrano-uploads-folder
# ==========================================================================

namespace :uploads do
  desc "Creates the upload folders unless they exist and sets the proper upload permissions"
  task :setup, :except => { :no_release => true } do
    dirs = uploads_dirs.map { |d| File.join(shared_path, d) }
    run "#{try_sudo} mkdir -p #{dirs.join(' ')} && #{try_sudo} chmod g+w #{dirs.join(' ')}"
  end

  desc "[internal] Creates the symlink to uploads shared folder for the most recently deployed version"
  task :symlink, :except => { :no_release => true } do
    run "rm -rf #{release_path}/public/uploads"
    run "ln -nfs #{shared_path}/uploads #{release_path}/public/uploads"
  end

  desc "[internal] Computes uploads directory paths and registers them in Capistrano environment"
  task :register_dirs do
    set :uploads_dirs,    %w(uploads)
    set :shared_children, fetch(:shared_children) + fetch(:uploads_dirs)
  end
end

after "deploy:update_code", "deploy:database_yml"
before "deploy:restart", "deploy:make_public"
after "deploy:finalize_update", "uploads:symlink"
on :start, "uploads:register_dirs"
