FactoryBot.define do
  factory :tutor do
    silo
    tutor_category
    name 'Tutor 1'
    subjects 'Rocket Science'
    contact '555-5555'
  end
end