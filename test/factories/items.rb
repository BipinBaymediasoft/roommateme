FactoryBot.define do
  factory :item do
    affiliate
    description 'Item 1 description'
    name 'Item 1 name'
    item_category

    trait :with_two_inventory_items do
      transient do
        inventory_items_count 2
      end

      after(:create) do |item, evaluator|
        create_list(:inventory_item, evaluator.inventory_items_count, item: item)
      end
    end
  end
end