FactoryBot.define do
  factory :silo do
    city 'City'
    state 'ST'

    factory :silo_with_universities do
      transient do
        universities_count 2
      end

      after(:create) do |silo, evaluator|
        create_list(:university, evaluator.universities_count, silo: silo)
      end
    end

    factory :silo_with_admins do
      transient do
        admin_users_count 2
      end

      after(:create) do |silo, evaluator|
        create_list(:admin_user, evaluator.admin_users_count, silo: silo)
      end
    end

    factory :silo_with_tutors do
      transient do
        tutors_count 2
      end

      after(:create) do |silo, evaluator|
        create_list(:tutor, evaluator.tutors_count, silo: silo)
      end
    end

    factory :silo_with_community_partners do
      transient do
        community_partners_count 2
      end

      after(:create) do |silo, evaluator|
        create_list(:community_partner, evaluator.community_partners_count, silo: silo)
      end
    end
  end
end