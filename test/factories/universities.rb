FactoryBot.define do
  factory :university do
    silo
    name 'Fake University'
    acronym 'FUNI'
    sequence :domain do |n|
      "fake#{n}.edu"
    end
    
    factory :university_with_users do
      transient do
        users_count 2
      end

      after(:create) do |university, evaluator|
        create_list(:user, evaluator.users_count, university: university)
      end
    end
  end
end