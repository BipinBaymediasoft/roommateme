FactoryBot.define do
  factory :friendship do
    user
    friend

    factory :accepted_friendship do
      status 'accepted'
    end

    factory :requested_friendship do
      status 'requested'
    end

    factory :pending_friendship do
      status 'pending'
    end
  end
end