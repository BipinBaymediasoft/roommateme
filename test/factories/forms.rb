FactoryBot.define do
  factory :form do
    title 'Form 1'
    instructions 'Fill in Form 1'

    factory :form_with_two_questions do
      transient do
        questions_count 2
      end

      after(:create) do |form, evaluator|
        create_list(:question, evaluator.questions_count, form: form)
      end
    end

    factory :form_with_two_responses do
      transient do
        responses_count 2
      end

      after(:create) do |form, evaluator|
        create_list(:response, evaluator.responses_count, form: form)
      end
    end
  end
end