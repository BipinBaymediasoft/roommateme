FactoryBot.define do
  factory :survey_response do
    user
    form

    trait :with_two_responses do
      transient do
        responses_count 2
      end

      after(:create) do |survey_response, evaluator|
        create_list(:response, evaluator.responses_count, survey_response: survey_response)
      end
    end
  end
end