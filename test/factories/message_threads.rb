FactoryBot.define do
  factory :message_thread do
    subject 'Message thread 1'
  end

  trait :with_two_messages do
    transient do
      messages_count 2
    end

    after(:create) do |message_thread, evaluator|
      create_list(:message, evaluator.messages_count, message_thread: message_thread)
    end
  end

  trait :with_two_participancies do
    transient do
      participancies_count 2
    end

    after(:create) do |message_thread, evaluator|
      create_list(:participancy, evaluator.participancies_count, message_thread: message_thread)
    end
  end
end