FactoryBot.define do
  factory :message do
    author
    message_thread
    body 'This is a message.'
  end
end