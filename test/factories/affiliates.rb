FactoryBot.define do
  factory :affiliate do
    name 'Affiliate 1'
    description 'Affiliate 1 description'
    affiliate_category

    factory :affiliate_with_two_items do
      transient do
        items_count 2
      end

      after(:create) do |affiliate, evaluator|
        create_list(:item, evaluator.items_count, affiliate: affiliate)
      end
    end
  end
end