FactoryBot.define do
  factory :community_partner do
    name 'Partner 1'
    description 'Partner 1 description'
    silo
    community_partner_category
  end
end