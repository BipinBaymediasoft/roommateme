FactoryBot.define do
  factory :community_partner_suggestion do
    name 'Partner 1'
    phone '555-5555'
    email 'partner@example.com'
    address '42 Wallaby Way, Sydney'
    silo
    patronage_frequency 'Everyday'
    reason 'Would make very good partner.'
  end
end