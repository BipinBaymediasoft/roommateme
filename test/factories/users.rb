FactoryBot.define do
  factory :user, aliases: [:friend, :user_1, :user_2, :author, :participant] do
    university
    sequence :email do |n|
      "user#{n}@#{university.domain}"
    end
    
    sequence :name do |n|
      "User #{n}"
    end

    password 'foobar'
    password_confirmation 'foobar'

    trait :with_two_responses do
      transient do
        responses_count 2
      end

      after(:create) do |user, evaluator|
        create_list(:response, evaluator.responses_count, user: user)
      end
    end

    trait :with_two_survey_responses do
      transient do
        survey_responses_count 2
      end

      after(:create) do |user, evaluator|
        create_list(:survey_response, evaluator.survey_responses_count, user: user)
      end
    end

    trait :with_two_matches do
      transient do
        matches_count 2
      end

      after(:create) do |user, evaluator|
        create_list(:match, evaluator.matches_count, user_1: user)
      end
    end

    trait :with_two_feedbacks do
      transient do
        feedbacks_count 2
      end

      after(:create) do |user, evaluator|
        create_list(:feedback, evaluator.feedbacks_count, user: user)
      end
    end

    trait :with_two_inventory_items do
      transient do
        inventory_items_count 2
      end

      after(:create) do |user, evaluator|
        create_list(:inventory_item, evaluator.inventory_items_count, user: user)
      end
    end

    trait :with_two_wanted_items do
      transient do
        wanted_items_count 2
      end

      after(:create) do |user, evaluator|
        create_list(:wanted_item, evaluator.wanted_items_count, user: user)
      end
    end

    trait :with_five_owned_items do
      transient do
        owned_items_count 5
      end

      after(:create) do |user, evaluator|
        create_list(:owned_item, evaluator.owned_items_count, user: user)
      end
    end

    trait :with_two_friendships do
      transient do
        friendships_count 2
      end

      after(:create) do |user, evaluator|
        create_list(:friendship, evaluator.friendships_count, user: user)
      end
    end

    trait :with_two_accepted_friendships do
      transient do
        accepted_friendships_count 2
      end

      after(:create) do |user, evaluator|
        create_list(:accepted_friendship, evaluator.accepted_friendships_count, user: user)
      end
    end

    trait :with_two_requested_friendships do
      transient do
        requested_friendships_count 2
      end

      after(:create) do |user, evaluator|
        create_list(:requested_friendship, evaluator.requested_friendships_count, user: user)
      end
    end

    trait :with_two_pending_friendships do
      transient do
        pending_friendships_count 2
      end

      after(:create) do |user, evaluator|
        create_list(:pending_friendship, evaluator.pending_friendships_count, user: user)
      end
    end

    trait :with_two_participancies do
      transient do
        participancies_count 2
      end

      after(:create) do |user, evaluator|
        create_list(:participancy, evaluator.participancies_count, user: user)
      end
    end

    trait :with_two_messages do
      transient do
        messages_count 2
      end

      after(:create) do |user, evaluator|
        create_list(:message, evaluator.messages_count, author_id: user.id)
      end
    end

    trait :with_flag_count do
      flag_count 2
    end


  end
end