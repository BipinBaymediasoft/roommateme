FactoryBot.define do
  factory :tutor_category do
    name 'Tutor Category 1'

    factory :tutor_category_with_tutors do
      transient do
        tutors_count 2
      end

      after(:create) do |tutor_category, evaluator|
        create_list(:tutor, evaluator.tutors_count, tutor_category: tutor_category)
      end
    end
  end
end