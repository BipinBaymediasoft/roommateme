FactoryBot.define do
  factory :answer do
    content 'Answer 1'
    question
    form
    points 10

    factory :answer_with_two_responses do
      transient do
        responses_count 2
      end

      after(:create) do |answer, evaluator|
        create_list(:response, evaluator.responses_count, answer: answer)
      end
    end
  end
end