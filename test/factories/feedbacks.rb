FactoryBot.define do
  factory :feedback do
    content 'Some useful feedback'

    factory :feedback_from_user do
      user
    end

    factory :feedback_with_category do
      feedback_category
    end
  end
end