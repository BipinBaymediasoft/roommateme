FactoryBot.define do
  factory :inventory_item do
    user
    item
    inventory_id { user.inventory.id }

    factory :wanted_item do
      want true
    end

    factory :owned_item do
      have true
    end
  end
end