FactoryBot.define do
  factory :response do
    user
    form
    question
    answer
  end
end