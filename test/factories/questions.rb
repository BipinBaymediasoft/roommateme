FactoryBot.define do
  factory :question do
    content 'Question 1'
    form
    weight 10

    factory :question_with_two_answers do
      transient do
        answers_count 2
      end

      after(:create) do |question, evaluator|
        create_list(:answer, evaluator.answers_count, question: question)
      end
    end

    factory :question_with_responses do
      transient do
        responses_count 2
      end

      after(:create) do |question, evaluator|
        create_list(:response, evaluator.responses_count, question: question)
      end
    end
  end
end