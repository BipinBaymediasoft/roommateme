FactoryBot.define do
  factory :feedback_category do
    name 'Feedback category 1'

    factory :internal_feedback_category do
      internal true
    end
  end
end