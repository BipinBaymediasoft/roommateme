FactoryBot.define do
  factory :affiliate_category do
    name 'Affiliate Category 1'

    factory :affiliate_category_with_two_affiliates do
      transient do
        affiliates_count 2
      end

      after(:create) do |affiliate_category, evaluator|
        create_list(:affiliate, evaluator.affiliates_count, affiliate_category: affiliate_category)
      end
    end
  end
end