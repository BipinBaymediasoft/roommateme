FactoryBot.define do
  factory :admin_user do
    sequence :email do |n|
      "admin.#{n}@example.com"
    end
    sequence :name do |n|
      "Admin #{n}"
    end
    password 'foobar'
    silo
  end
end