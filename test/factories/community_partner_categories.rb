FactoryBot.define do
  factory :community_partner_category do
    name 'Community Partner 1'

    factory :community_partner_category_with_two_partners do
      transient do
        community_partners_count 2
      end

      after(:create) do |community_partner_category, evaluator|
        create_list(:community_partner, evaluator.community_partners_count, community_partner_category: community_partner_category)
      end
    end
  end
end