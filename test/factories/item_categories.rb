FactoryBot.define do
  factory :item_category do
    name 'Item category 1'
  
    factory :item_category_with_two_items do
      transient do
        items_count 2
      end

      after(:create) do |item_category, evaluator|
        create_list(:item, evaluator.items_count, item_category: item_category)
      end
    end
  end
end