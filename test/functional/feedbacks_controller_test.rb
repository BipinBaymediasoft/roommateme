require 'test_helper'

class FeedbacksControllerTest < ActionController::TestCase
  test "should get report" do
    get :report
    assert_response :success
  end

  test "should get flag" do
    get :flag
    assert_response :success
  end

end
