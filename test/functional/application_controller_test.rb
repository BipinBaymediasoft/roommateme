require 'test_helper'

class ApplicationControllerTest < ActionController::TestCase
  


  test 'should get feedback categories for footer' do
    category = create(:feedback_category)
    internal_category = create(:internal_feedback_category)
    
    assert_includes @controller.feedback_categories_for_footer, category
    assert_not_includes @controller.feedback_categories_for_footer, 
      internal_category
  end
end