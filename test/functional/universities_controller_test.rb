require 'test_helper'

class UniversitiesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get show" do
    get :show, id: universities(:one).id
    assert_response :success
  end

end
