require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  test "should get new" do
    get :new
    assert_response :success
    assert_not_nil assigns(:user), "New user object not assigned"
  end

  test 'should get user profile if signed in' do
    user = create(:user)
    log_in_as(user)
    get :new
    assert_redirected_to root_url, "Not redirected to user profile"
    assert_not_nil flash[:notice], "Did not display user notice"
  end

end
