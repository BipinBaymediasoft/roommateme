require 'test_helper'

class CommunityPartnerSuggestionsControllerTest < ActionController::TestCase
  setup do
    @community_partner_suggestion = community_partner_suggestions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:community_partner_suggestions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create community_partner_suggestion" do
    assert_difference('CommunityPartnerSuggestion.count') do
      post :create, :community_partner_suggestion => @community_partner_suggestion.attributes
    end

    assert_redirected_to community_partner_suggestion_path(assigns(:community_partner_suggestion))
  end

  test "should show community_partner_suggestion" do
    get :show, :id => @community_partner_suggestion.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @community_partner_suggestion.to_param
    assert_response :success
  end

  test "should update community_partner_suggestion" do
    put :update, :id => @community_partner_suggestion.to_param, :community_partner_suggestion => @community_partner_suggestion.attributes
    assert_redirected_to community_partner_suggestion_path(assigns(:community_partner_suggestion))
  end

  test "should destroy community_partner_suggestion" do
    assert_difference('CommunityPartnerSuggestion.count', -1) do
      delete :destroy, :id => @community_partner_suggestion.to_param
    end

    assert_redirected_to community_partner_suggestions_path
  end
end
