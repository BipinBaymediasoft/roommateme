require 'test_helper'

class ResponseTest < ActiveSupport::TestCase
  
  test 'should create valid Response' do
    assert_difference 'Response.count' do
      response = create(:response)
    end
  end

  test 'should delete valid Response' do
    response = create(:response)
    assert_difference 'Response.count', -1 do
      Response.delete(response)
    end
  end

  test 'should not save Response without user_id' do
    response = build(:response, user: nil)
    assert_no_difference 'Response.count', 'Saved Response without user_id' do
      response.save
    end 
    assert_not_empty response.errors[:user_id], 'No validation error for user_id present'
  end

  test 'should not save Response without form_id' do
    response = build(:response, form: nil)
    assert_no_difference 'Response.count', 'Saved Response without form_id' do
      response.save
    end 
    assert_not_empty response.errors[:form_id], 'No validation error for form_id present'
  end

  test 'should not save Response without question_id' do
    response = build(:response, question: nil)
    assert_no_difference 'Response.count', 'Saved Response without question_id' do
      response.save
    end 
    assert_not_empty response.errors[:question_id], 'No validation error for question_id present'
  end

  test 'should not save Response without answer_id' do
    response = build(:response, answer: nil)
    assert_no_difference 'Response.count', 'Saved Response without answer_id' do
      response.save
    end 
    assert_not_empty response.errors[:answer_id], 'No validation error for answer_id present'
  end
end
