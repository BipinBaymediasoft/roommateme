require 'test_helper'

class SurveyResponseTest < ActiveSupport::TestCase
  
  test 'should create valid SurveyResponse' do
    assert_difference 'SurveyResponse.count' do
      response = create(:survey_response)
    end
  end

  test 'should delete valid Response' do
    response = create(:survey_response)
    assert_difference 'SurveyResponse.count', -1 do
      SurveyResponse.delete(response)
    end
  end

  test 'should not save SurveyResponse without user_id' do
    response = build(:survey_response, user: nil)
    assert_no_difference 'SurveyResponse.count', 'Saved SurveyResponse without user_id' do
      response.save
    end 
    assert_not_empty response.errors[:user_id], 'No validation error for user_id present'
  end

  test 'should not save SurveyResponse without form_id' do
    response = build(:survey_response, form: nil)
    assert_no_difference 'SurveyResponse.count', 'Saved SurveyResponse without form_id' do
      response.save
    end 
    assert_not_empty response.errors[:form_id], 'No validation error for form_id present'
  end

  # test 'should have many responses' do
  #   response = create(:survey_response, :with_two_responses)
  #   assert_equal 2, response.responses.size
  # end

  test 'should update responses, create user matches and get 75% match' do
    # Create users 
    silo = create(:silo)
    uni = create(:university, silo: silo)
    user1 = create(:user, email: "john@#{uni.domain}")
    user2 = create(:user, email: "jane@#{uni.domain}")
    
    # Create form
    form = create(:form)
    question = create(:question, form: form)
    answer1 = create(:answer, form: form, question: question, points: 1)
    answer2 = create(:answer, form: form, question: question, points: 2)
    
    # Responses
    response_user1 = create(:response, user: user1, form: form, question: question, answer: question.answers.first)
    response_user2 = create(:response, user: user2, form: form, question: question, answer: question.answers.last)

    # Survey responses
    survey_response1 = create(:survey_response, user: user1, form: form)
    survey_response1.responses << user1.responses.first
    survey_response2 = create(:survey_response, user: user2, form: form)
    survey_response2.responses << user2.responses.first
    

    # Get matches
    survey_response1.update_responses
    assert 75, user1.matches.first.percent
  end
end
