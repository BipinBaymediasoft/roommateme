require 'test_helper'

class CommunityPartnerSuggestionTest < ActiveSupport::TestCase

  test 'should create valid CommunityPartnerSuggestion' do
    assert_difference 'CommunityPartnerSuggestion.count' do
      suggestion = create(:community_partner_suggestion)
    end
  end

  test 'should delete valid CommunityPartnerSuggestion' do
    suggestion = create(:community_partner_suggestion)
    assert_difference 'CommunityPartnerSuggestion.count', -1 do
      CommunityPartnerSuggestion.delete(suggestion)
    end
  end

  test 'should not save CommunityPartnerSuggestion without name' do
    suggestion = build(:community_partner_suggestion, name: '')
    assert_no_difference 'CommunityPartnerSuggestion.count', 'Saved CommunityPartnerSuggestion without name' do
      suggestion.save
    end 
    assert_not_empty suggestion.errors[:name], 'No validation error for name present'
  end

  test 'should not save CommunityPartnerSuggestion without phone' do
    suggestion = build(:community_partner_suggestion, phone: nil)
    assert_no_difference 'CommunityPartnerSuggestion.count', 'Saved CommunityPartnerSuggestion without phone' do
      suggestion.save
    end 
    assert_not_empty suggestion.errors[:phone], 'No validation error for phone present'
  end

  test 'should not save CommunityPartnerSuggestion without email' do
    suggestion = build(:community_partner_suggestion, email: nil)
    assert_no_difference 'CommunityPartnerSuggestion.count', 'Saved CommunityPartnerSuggestion without email' do
      suggestion.save
    end 
    assert_not_empty suggestion.errors[:email], 'No validation error for email present'
  end

  test 'should not save CommunityPartnerSuggestion without address' do
    suggestion = build(:community_partner_suggestion, address: nil)
    assert_no_difference 'CommunityPartnerSuggestion.count', 'Saved CommunityPartnerSuggestion without address' do
      suggestion.save
    end 
    assert_not_empty suggestion.errors[:address], 'No validation error for address present'
  end

  test 'should not save CommunityPartnerSuggestion without silo_id' do
    suggestion = build(:community_partner_suggestion, silo: nil)
    assert_no_difference 'CommunityPartnerSuggestion.count', 'Saved CommunityPartnerSuggestion without silo_id' do
      suggestion.save
    end 
    assert_not_empty suggestion.errors[:silo_id], 'No validation error for silo_id present'
  end

  test 'should not save CommunityPartnerSuggestion without patronage_frequency' do
    suggestion = build(:community_partner_suggestion, patronage_frequency: nil)
    assert_no_difference 'CommunityPartnerSuggestion.count', 'Saved CommunityPartnerSuggestion without patronage_frequency' do
      suggestion.save
    end 
    assert_not_empty suggestion.errors[:patronage_frequency], 'No validation error for patronage_frequency present'
  end

  test 'should not save CommunityPartnerSuggestion without reason' do
    suggestion = build(:community_partner_suggestion, reason: '')
    assert_no_difference 'CommunityPartnerSuggestion.count', 'Saved CommunityPartnerSuggestion without reason' do
      suggestion.save
    end 
    assert_not_empty suggestion.errors[:reason], 'No validation error for reason present'
  end
 
end
