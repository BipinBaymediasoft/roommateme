require 'test_helper'

class FormTest < ActiveSupport::TestCase
  
  test 'should create valid Form' do
    assert_difference 'Form.count' do
      form = create(:form)
    end
  end

  test 'should delete valid Form' do
    form = create(:form)
    assert_difference 'Form.count', -1 do
      Form.delete(form)
    end
  end

  test 'should not save Form without title' do
    form = build(:form, title: nil)
    assert_no_difference 'Form.count', 'Saved Form without title' do
      form.save
    end 
    assert_not_empty form.errors[:title], 'No validation error for title present'
  end

  test 'should not save Form without instructions' do
    form = build(:form, instructions: nil)
    assert_no_difference 'Form.count', 'Saved Form without instructions' do
      form.save
    end 
    assert_not_empty form.errors[:instructions], 'No validation error for instructions present'
  end

  test 'should have many Questions' do
    form = create(:form_with_two_questions)
    assert_equal 2, form.questions.length
  end

  test 'should have many Answers through Questions' do
    form = create(:form)
    question = create(:question_with_two_answers, form: form)
    assert_equal 2, form.answers.length
  end

  test 'should have many responses' do
    form = create(:form_with_two_responses)
    assert_equal 2, form.responses.length
  end

  test 'should return Form name' do
    form = create(:form)
    assert_equal "Survey Version ##{form.id} (F#{form.id})", form.name, "Returned wrong Form name"
  end

  # Should collect users responses
  test 'should collect users responses' do
    user = create(:user)
    form = create(:form)
    response1 = create(:response, user: user, form: form)
    response2 = create(:response, user: user, form: form)
    assert_equal 2, form.collect_users_responses(user).length
  end

  # test 'should collect users answered questins' do
  #   user = create(:user)
  #   form = create(:form)
  #   3.times { create(:question, form: form) }
  #   answer1 = create(:answer)
  # end

  test 'should return true if form completed by user' do
    user = create(:user)
    form = create(:form)
    2.times { create(:question, form: form) }
    response1 = create(:response, user: user, form: form, question: form.questions.first)
    response2 = create(:response, user: user, form: form, question: form.questions.last)
    assert form.completed_by_user?(user)
  end

  test 'should return true if form started by user' do
    user = create(:user)
    form = create(:form)
    2.times { create(:question, form: form) }
    response1 = create(:response, user: user, form: form, question: form.questions.first)
    assert form.started_by_user?(user)
  end

  test 'should return index of last answered question' do
    user = create(:user)
    form = create(:form_with_two_questions)
    response = create(:response, user: user, form: form, question: form.questions.first)
    assert_equal 1, form.last_answered_question_index(user)
  end
end