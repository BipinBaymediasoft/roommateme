require 'test_helper'

class AdminUserTest < ActiveSupport::TestCase

  test 'should create valid AdminUser' do
    assert_difference 'AdminUser.count' do
      admin = create(:admin_user)
    end
  end

  test 'should delete valid AdminUser' do
    admin = create(:admin_user)
    assert_difference 'AdminUser.count', -1 do
      AdminUser.delete(admin)
    end
  end

  test 'should not save AdminUser without email' do
    admin = build(:admin_user, email: nil)
    assert_no_difference 'AdminUser.count', 'Saved AdminUser without email' do
      admin.save
    end 
    assert_not_empty admin.errors[:email], 'No validation error for email present'
  end

  test 'should not save AdminUser without name' do
    admin = build(:admin_user, name: nil)
    assert_no_difference 'AdminUser.count', 'Saved AdminUser without name' do
      admin.save
    end 
    assert_not_empty admin.errors[:name], 'No validation error for name present'
  end

  test 'should not save AdminUser without silo_id' do
    admin = build(:admin_user, silo: nil)
    assert_no_difference 'AdminUser.count', 'Saved AdminUser without silo_id' do
      admin.save
    end 
    assert_not_empty admin.errors[:silo_id], 'No validation error for silo_id present'
  end
end
