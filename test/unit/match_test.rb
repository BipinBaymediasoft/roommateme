require 'test_helper'

class MatchTest < ActiveSupport::TestCase
  
  test 'should create valid Match' do
    assert_difference 'Match.count' do
      match = create(:match)
    end
  end

  test 'should delete valid Match' do
    match = create(:match)
    assert_difference 'Match.count', -1 do
      Match.delete(match)
    end
  end

  test 'should not save Match without user_1_id' do
    match = build(:match, user_1: nil)
    assert_no_difference 'Match.count', 'Saved Match without user_1_id' do
      match.save
    end 
    assert_not_empty match.errors[:user_1_id], 'No validation error for user_1_id present'
  end

   test 'should not save Match without user_2_id' do
    match = build(:match, user_2: nil)
    assert_no_difference 'Match.count', 'Saved Match without user_2_id' do
      match.save
    end 
    assert_not_empty match.errors[:user_2_id], 'No validation error for user_2_id present'
  end

   test 'should not save Match without percent' do
    match = build(:match, percent: nil)
    assert_no_difference 'Match.count', 'Saved Match without percent' do
      match.save
    end 
    assert_not_empty match.errors[:percent], 'No validation error for percent present'
  end
end
