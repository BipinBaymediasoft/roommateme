require 'test_helper'

class ParticipancyTest < ActiveSupport::TestCase
  
  test 'should create valid Participancy' do
    assert_difference 'Participancy.count' do
      participancy = create(:participancy)
    end
  end

  test 'should delete valid Participancy' do
    participancy = create(:participancy)
    assert_difference 'Participancy.count', -1 do
      Participancy.delete(participancy)
    end
  end

  test 'should not save Participancy without user_id' do
    participancy = build(:participancy, user: nil)
    assert_no_difference 'Participancy.count', 'Saved Participancy without user_id' do
      participancy.save
    end 
    assert_not_empty participancy.errors[:user_id], 'No validation error for user_id present'
  end

  test 'should not save Participancy without thread_id' do
    participancy = build(:participancy, message_thread: nil)
    assert_no_difference 'Participancy.count', 'Saved Participancy without thread_id' do
      participancy.save
    end 
    assert_not_empty participancy.errors[:thread_id], 'No validation error for thread_id present'
  end
end
