require 'test_helper'

class AffiliateTest < ActiveSupport::TestCase

  test 'should create valid Affiliate' do
    assert_difference 'Affiliate.count' do
      affiliate = create(:affiliate)
    end
  end

  test 'should delete valid Affiliate' do
    affiliate = create(:affiliate)
    assert_difference 'Affiliate.count', -1 do
      Affiliate.delete(affiliate)
    end
  end

  test 'should not save Affiliate without name' do
    affiliate = build(:affiliate, name: nil)
    assert_no_difference 'Affiliate.count', 'Saved Affiliate without name' do
      affiliate.save
    end 
    assert_not_empty affiliate.errors[:name], 'No validation error for name present'
  end

  test 'should not save Affiliate without description' do
    affiliate = build(:affiliate, description: nil)
    assert_no_difference 'Affiliate.count', 'Saved Affiliate without description' do
      affiliate.save
    end 
    assert_not_empty affiliate.errors[:description], 'No validation error for name present'
  end
  
  test 'should not save Affiliate without affiliate_category_id' do
    affiliate = build(:affiliate, affiliate_category: nil)
    assert_no_difference 'Affiliate.count', 'Saved Affiliate without affiliate_category_id' do
      affiliate.save
    end 
    assert_not_empty affiliate.errors[:affiliate_category_id], 'No validation error for affiliate_category_id present'
  end

  test 'should have many items' do
    affiliate = create(:affiliate_with_two_items)
    assert_equal 2, affiliate.items.length
  end

  test 'should search Affiliate by term' do
    affiliate = create(:affiliate)
    search_results = Affiliate.search(affiliate.name)
    assert_includes search_results, affiliate, "Couldn't find affiliate by terms"
  end
  
end
