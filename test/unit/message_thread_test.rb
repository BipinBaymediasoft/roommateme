require 'test_helper'

class MessageThreadTest < ActiveSupport::TestCase
  
  test 'should create valid MessageThread' do
    assert_difference 'MessageThread.count' do
      thread = create(:message_thread)
    end
  end

  test 'should delete valid MessageThread' do
    thread = create(:message_thread)
    assert_difference 'MessageThread.count', -1 do
      MessageThread.delete(thread)
    end
  end

  test 'should not save MessageThread without subject' do
    thread = build(:message_thread, subject: nil)
    assert_no_difference 'MessageThread.count', 'Saved MessageThread without subject' do
      thread.save
    end 
    assert_not_empty thread.errors[:subject], 'No validation error for subject present'
  end

  test 'should have many Messages' do
    thread = create(:message_thread, :with_two_messages)
    assert_equal 2, thread.messages.length
  end

  test 'should have many Participancies' do
    thread = create(:message_thread, :with_two_participancies)
    assert_equal 2, thread.participancies.length
  end

  test 'should have many Participants through Participancies' do
    thread = create(:message_thread, :with_two_participancies)
    assert_equal 2, thread.participants.length
  end
end
