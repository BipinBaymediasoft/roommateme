require 'test_helper'

class CommunityPartnerTest < ActiveSupport::TestCase
  
  test 'should create valid CommunityPartner' do
    assert_difference 'CommunityPartner.count' do
      partner = create(:community_partner)
    end
  end

  test 'should delete valid CommunityPartner' do
    partner = create(:community_partner)
    assert_difference 'CommunityPartner.count', -1 do
      CommunityPartner.delete(partner)
    end
  end

  test 'should not save CommunityPartner without name' do
    partner = build(:community_partner, name: nil)
    assert_no_difference 'CommunityPartner.count', 'Saved CommunityPartner without name' do
      partner.save
    end 
    assert_not_empty partner.errors[:name], 'No validation error for name present'
  end

  test 'should not save CommunityPartner without description' do
    partner = build(:community_partner, description: nil)
    assert_no_difference 'CommunityPartner.count', 'Saved CommunityPartner without description' do
      partner.save
    end 
    assert_not_empty partner.errors[:description], 'No validation error for description present'
  end

  test 'should not save CommunityPartner without silo_id' do
    partner = build(:community_partner, silo: nil)
    assert_no_difference 'CommunityPartner.count', 'Saved CommunityPartner without silo_id' do
      partner.save
    end 
    assert_not_empty partner.errors[:silo_id], 'No validation error for silo_id present'
  end

  test 'should not save CommunityPartner without community_partner_category_id' do
    partner = build(:community_partner, community_partner_category: nil)
    assert_no_difference 'CommunityPartner.count', 'Saved CommunityPartner without community_partner_category_id' do
      partner.save
    end 
    assert_not_empty partner.errors[:community_partner_category_id], 'No validation error for community_partner_category_id present'
  end

  test 'should search CommunityPartner by terms and silo_id' do
    partner = create(:community_partner)
    search_results = CommunityPartner.search(partner.description, partner.silo_id)
    assert_includes search_results, partner, "Couldn't find CommunityPartner by terms and silo_id"
  end

  test 'should return random community partner from silo' do
    silo = create(:silo)
    partner1 = create(:community_partner, silo: silo)
    partner2 = create(:community_partner, silo: silo)
    partner3 = create(:community_partner, silo: silo)
    partners = [partner1, partner2, partner3]
    assert_includes partners, partner1.random_cp_from(silo).first
  end
end
