require 'test_helper'

class AffiliateCategoryTest < ActiveSupport::TestCase

  test 'should create valid AffiliateCategory' do
    assert_difference 'AffiliateCategory.count' do
      category = create(:affiliate_category)
    end
  end

  test 'should delete valid AffiliateCategory' do
    category = create(:affiliate_category)
    assert_difference 'AffiliateCategory.count', -1 do
      AffiliateCategory.delete(category)
    end
  end

  test 'should not save AffiliateCategory without name' do
    category = build(:affiliate_category, name: nil)
    assert_no_difference 'AffiliateCategory.count', 'Saved AffiliateCategory without name' do
      category.save
    end 
    assert_not_empty category.errors[:name], 'No validation error for name present'
  end

  test 'should have many Affiliates' do
    category = create(:affiliate_category_with_two_affiliates)
    assert_equal 2, category.affiliates.length
  end
end
