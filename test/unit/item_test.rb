require 'test_helper'

class ItemTest < ActiveSupport::TestCase
  
  test 'should create valid Item' do
    assert_difference 'Item.count' do
      item = create(:item)
    end
  end

  test 'should delete valid Item' do
    item = create(:item)
    assert_difference 'Item.count', -1 do
      Item.delete(item)
    end
  end

  test 'should not save Item without affiliate_id' do
    item = build(:item, affiliate: nil)
    assert_no_difference 'Item.count', 'Saved Item without affiliate_id' do
      item.save
    end 
    assert_not_empty item.errors[:affiliate_id], 'No validation error for affiliate_id present'
  end

  test 'should not save Item without description' do
    item = build(:item, description: nil)
    assert_no_difference 'Item.count', 'Saved Item without description' do
      item.save
    end 
    assert_not_empty item.errors[:description], 'No validation error for description present'
  end

  test 'should not save Item without name' do
    item = build(:item, name: nil)
    assert_no_difference 'Item.count', 'Saved Item without name' do
      item.save
    end 
    assert_not_empty item.errors[:name], 'No validation error for name present'
  end

  test 'should not save Item without item_category_id' do
    item = build(:item, item_category: nil)
    assert_no_difference 'Item.count', 'Saved Item without item_category_id' do
      item.save
    end 
    assert_not_empty item.errors[:item_category_id], 'No validation error for item_category_id present'
  end

  test 'has many InventoryItems' do
    item = create(:item, :with_two_inventory_items)
    assert_equal 2, item.inventory_items.length
  end

  test 'has many Users through InventoryItems' do
    item = create(:item, :with_two_inventory_items)
    assert_equal 2, item.users.length
  end

  test 'should search Item by terms' do
    item = create(:item)
    search_results = Item.search(item.description)
    assert_includes search_results, item, "Couldn't find item by terms"
  end
end
