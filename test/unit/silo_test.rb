require 'test_helper'

class SiloTest < ActiveSupport::TestCase
  
  test 'should create valid Silo' do
    assert_difference 'Silo.count' do
      silo = create(:silo)
    end
  end

  test 'should delete valid Silo' do
    silo = create(:silo)
    assert_difference 'Silo.count', -1 do
      Silo.delete(silo)
    end
  end

  test 'should not save Silo without city' do
    silo = build(:silo, city: nil)
    assert_no_difference 'Silo.count', 'Saved Silo without city' do
      silo.save
    end 
    assert_not_empty silo.errors[:city], 'No validation error for city present'
  end

  test 'should not save Silo without state' do
    silo = build(:silo, state: nil)
    assert_no_difference 'Silo.count', 'Saved Silo without state' do
      silo.save
    end 
    assert_not_empty silo.errors[:state], 'No validation error for state present'
  end

  test 'should generate name' do
    silo = create(:silo, city: 'New York', state: 'NY')
    assert_equal 'New York, NY', silo.name, 'Generated wrong silo name'
  end

  test 'should have many univeristies' do
    silo = create(:silo_with_universities)
    assert_equal 2, silo.universities.length
  end

  test 'should have many users through universities' do
    silo = create(:silo)
    uni = create(:university_with_users, silo: silo)
    assert_equal 2, silo.users.length
  end

  test 'should have many admin_users' do
    silo = create(:silo_with_admins)
    assert_equal 2, silo.admin_users.length
  end

  test 'should have many community_partners' do
    silo = create(:silo_with_community_partners)
    assert_equal 2, silo.community_partners.length
  end

  test 'should have many tutors' do
    silo = create(:silo_with_tutors)
    assert_equal 2, silo.tutors.size
  end
end
