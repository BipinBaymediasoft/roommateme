require 'test_helper'

class TutorTest < ActiveSupport::TestCase
  
  test 'should create valid Tutor' do
    assert_difference 'Tutor.count' do
      tutor = create(:tutor)
    end
  end

  test 'should delete valid Tutor' do
    tutor = create(:tutor)
    assert_difference 'Tutor.count', -1 do
      Tutor.delete(tutor)
    end
  end

  test 'should not save Tutor without name' do
    tutor = build(:tutor, name: nil)
    assert_no_difference 'Tutor.count', 'Saved Tutor without name' do
      tutor.save
    end 
    assert_not_empty tutor.errors[:name], 'No validation error for name present'
  end

  test 'should not save Tutor without subjects' do
    tutor = build(:tutor, subjects: nil)
    assert_no_difference 'Tutor.count', 'Saved Tutor without subjects' do
      tutor.save
    end 
    assert_not_empty tutor.errors[:subjects], 'No validation error for subjects present'
  end

  test 'should not save Tutor without contact' do
    tutor = build(:tutor, contact: nil)
    assert_no_difference 'Tutor.count', 'Saved Tutor without contact' do
      tutor.save
    end 
    assert_not_empty tutor.errors[:contact], 'No validation error for contact present'
  end

  test 'should not save Tutor without silo_id' do
    tutor = build(:tutor, silo: nil)
    assert_no_difference 'Tutor.count', 'Saved Tutor without silo_id' do
      tutor.save
    end 
    assert_not_empty tutor.errors[:silo_id], 'No validation error for silo_id present'
  end

  test 'should not save Tutor without tutor_category_id' do
    tutor = build(:tutor, tutor_category: nil)
    assert_no_difference 'Tutor.count', 'Saved Tutor without tutor_category_id' do
      tutor.save
    end 
    assert_not_empty tutor.errors[:tutor_category_id], 'No validation error for tutor_category_id present'
  end
end
