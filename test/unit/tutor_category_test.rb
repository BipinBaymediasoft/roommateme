require 'test_helper'

class TutorCategoryTest < ActiveSupport::TestCase
  
  test 'should create valid TutorCategory' do
    assert_difference 'TutorCategory.count' do
      category = create(:tutor_category)
    end
  end

  test 'should delete valid TutorCategory' do
    category = create(:tutor_category)
    assert_difference 'TutorCategory.count', -1 do
      TutorCategory.delete(category)
    end
  end

  test 'should not save TutorCategory without name' do
    category = build(:tutor_category, name: nil)
    assert_no_difference 'TutorCategory.count', 'Saved TutorCategory without name' do
      category.save
    end 
    assert_not_empty category.errors[:name], 'No validation error for user_id present'
  end

  test 'should_have_many_tutors' do
    category = create(:tutor_category_with_tutors)
    assert_equal 2, category.tutors.length
  end
end
