require 'test_helper'

class MessageTest < ActiveSupport::TestCase
  
  test 'should create valid Message' do
    assert_difference 'Message.count' do
      message = create(:message)
    end
  end

  test 'should delete valid Message' do
    message = create(:message)
    assert_difference 'Message.count', -1 do
      Message.delete(message)
    end
  end

  test 'should not save Message without body' do
    message = build(:message, body: nil)
    assert_no_difference 'Message.count', 'Saved Message without body' do
      message.save
    end 
    assert_not_empty message.errors[:body], 'No validation error for body present'
  end

  test 'should not save Message without message_thread_id' do
    message = build(:message, message_thread: nil)
    assert_no_difference 'Message.count', 'Saved Message without message_thread_id' do
      message.save
    end 
    assert_not_empty message.errors[:message_thread_id], 'No validation error for message_thread_id present'
  end

  test 'should not save Message without author_id' do
    message = build(:message, author: nil)
    assert_no_difference 'Message.count', 'Saved Message without author_id' do
      message.save
    end 
    assert_not_empty message.errors[:author_id], 'No validation error for author_id present'
  end
end
