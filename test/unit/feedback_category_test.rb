require 'test_helper'

class FeedbackCategoryTest < ActiveSupport::TestCase
  
  test 'should create valid FeedbackCategory' do
    assert_difference 'FeedbackCategory.count' do
      category = create(:feedback_category)
    end
  end

  test 'should delete valid FeedbackCategory' do
    category = create(:feedback_category)
    assert_difference 'FeedbackCategory.count', -1 do
      FeedbackCategory.delete(category)
    end
  end

  test 'should not save FeedbackCategory without name' do
    category = build(:feedback_category, name: nil)
    assert_no_difference 'FeedbackCategory.count', 'Saved FeedbackCategory without name' do
      category.save
    end 
    assert_not_empty category.errors[:name], 'No validation error for name present'
  end

  test 'should return all_unless_internal' do
    category_not_internal = create(:feedback_category)
    category_internal = create(:internal_feedback_category)
    assert_includes FeedbackCategory.all_unless_internal, category_not_internal
    assert_not_includes FeedbackCategory.all_unless_internal, category_internal 
  end
end
