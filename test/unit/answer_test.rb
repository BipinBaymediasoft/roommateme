require 'test_helper'

class AnswerTest < ActiveSupport::TestCase

  test 'should create valid Answer' do
    assert_difference 'Answer.count' do
      answer = create(:answer)
    end
  end

  test 'should delete valid Answer' do
    answer = create(:answer)
    assert_difference 'Answer.count', -1 do
      Answer.delete(answer)
    end
  end

  test 'should not save Answer without content' do
    answer = build(:answer, content: nil)
    assert_no_difference 'Answer.count', 'Saved Answer without content' do
      answer.save
    end 
    assert_not_empty answer.errors[:content], 'No validation error for content present'
  end

  test 'should not save Answer without question_id' do
    answer = build(:answer, question: nil)
    assert_no_difference 'Answer.count', 'Saved Answer without question_id' do
      answer.save
    end 
    assert_not_empty answer.errors[:question_id], 'No validation error for question_id present'
  end

  test 'should not save Answer without form_id' do
    answer = build(:answer, form: nil)
    assert_no_difference 'Answer.count', 'Saved Answer without form_id' do
      answer.save
    end 
    assert_not_empty answer.errors[:form_id], 'No validation error for form_id present'
  end

  test 'should not save Answer without points' do
    answer = build(:answer, points: nil)
    assert_no_difference 'Answer.count', 'Saved Answer without points' do
      answer.save
    end 
    assert_not_empty answer.errors[:points], 'No validation error for points present'
  end

  test 'should have many responses' do
    answer = create(:answer_with_two_responses)
    assert_equal 2, answer.responses.length
  end

  test 'should return answer name' do
    answer = create(:answer)
    assert_equal "Answer ##{answer.id} Q#{answer.question.id} F#{answer.question.form.id}", answer.name, "Returned wrong answer name"
  end
  
  test 'should return is not response?' do
    user = create(:user)
    answer = create(:answer)
    answer2 = create(:answer, content: 'Different answer')
    response = create(:response, user: user, form: answer2.form, question: answer2.question, answer: answer2)
    assert answer.is_not_response?(user, answer.form, answer.question)
  end
  
  test 'should return is response?' do
    user = create(:user)
    answer = create(:answer)
    response = create(:response, user: user, form: answer.form, question: answer.question, answer: answer)
    assert answer.is_response?(user, answer.form, answer.question)
  end

end
