require 'test_helper'

class UserTest < ActiveSupport::TestCase
  
  test 'should create valid User' do
    assert_difference 'User.count' do
      user = create(:user)
    end
  end

  test 'should delete valid User' do
    user = create(:user)
    assert_difference 'User.count', -1 do
      User.delete(user)
    end
  end

  test 'should not save User without email' do
    user = build(:user, email: nil)
    assert_no_difference 'User.count', 'Saved User without email' do
      user.save
    end 
    assert_not_empty user.errors[:email], 'No validation error for email present'
  end

  test 'should not save User without unique email' do
    user = create(:user)
    user2 = build(:user, email: user.email)
    assert_no_difference 'User.count', 'Saved User without unique email' do
      user2.save
    end 
    assert_not_empty user2.errors[:email], 'No validation error for email uniqueness present'
  end

  test 'should not save User without name' do
    user = build(:user, name: nil)
    assert_no_difference 'User.count', 'Saved User without name' do
      user.save
    end 
    assert_not_empty user.errors[:name], 'No validation error for name present'
  end

  test 'should not save User without password' do
    user = build(:user, password: nil)
    assert_no_difference 'User.count', 'Saved User without password' do
      user.save
    end
  end

  test 'should not save User without password confirmation' do
    user = build(:user, password_confirmation: '')
    assert_no_difference 'User.count', 'Saved User without password confirmation' do
      user.save
    end
    assert_not_empty user.errors[:password_confirmation], 'No validation errors for password confirmation'
  end

  # Associations
  test 'should have many Responses' do
    user = create(:user, :with_two_responses)
    assert_equal 2, user.responses.length
  end

  test 'should have many SurveyResponses' do
    user = create(:user, :with_two_survey_responses)
    assert_equal 2, user.survey_responses.length
  end

  test 'should have many Matches' do
    user = create(:user, :with_two_matches)
    assert_equal 2, user.matches.length
  end

  test 'should have many Feedbacks' do
    user = create(:user, :with_two_feedbacks)
    assert_equal 2, user.feedbacks.length
  end

  test 'should have many InventoryItems' do
    user = create(:user, :with_two_inventory_items)
    assert_equal 2, user.inventory_items.length
  end

  test 'should have many wanted items' do
    user = create(:user, :with_two_wanted_items)
    assert_equal 2, user.wanted_items.length
  end

  test 'should have many owned items' do
    user = create(:user, :with_five_owned_items)
    assert_equal 5, user.owned_items.length
  end

  test 'should have many Friendships' do
    user = create(:user, :with_two_friendships)
    assert_equal 2, user.friendships.length
  end

  test 'should have many friends' do
    user = create(:user, :with_two_accepted_friendships)
    assert_equal 2, user.friends.length
  end

  test 'should have many requested friends' do
    user = create(:user, :with_two_requested_friendships)
    assert_equal 2, user.requested_friends.length
  end

  test 'should have many pending friends' do
    user = create(:user, :with_two_pending_friendships)
    assert_equal 2, user.pending_friends.length
  end

  test 'should have many Participancies' do
    user = create(:user, :with_two_participancies)
    assert_equal 2, user.participancies.length
  end

  test 'should have many MessageThreads through Participancies' do
    user = create(:user)
    2.times { create(:participancy, user: user) }
    assert_equal 2, user.message_threads.length
  end

  test 'should have many Messages' do
    user = create(:user, :with_two_messages)
    assert_equal 2, user.messages.length
  end

  # Format validations 
  test 'should validate Facebook URL' do
    user = build(:user, facebook: 'foobar')
    assert_not user.valid?, 'Passed invalid Facebook URL'
    assert_not_empty user.errors[:facebook], 'No validation error for facebook present'
  end

  test 'should validate AIM URL' do
    user = build(:user, aim: 'foobar')
    assert_not user.valid?, 'Passed invalid AIM URL'
    assert_not_empty user.errors[:aim], 'No validation error for AIM present'
  end

  test 'should validate Yahoo URL' do
    user = build(:user, yahoo: 'foobar')
    assert_not user.valid?, 'Passed invalid Yahoo URL'
    assert_not_empty user.errors[:yahoo], 'No validation error for Yahoo present'
  end

  test 'should validate MySpace URL' do
    user = build(:user, myspace: 'foobar')
    assert_not user.valid?, 'Passed invalid MySpace URL'
    assert_not_empty user.errors[:myspace], 'No validation error for MySpace present'
  end

  test 'should validate MSN URL' do
    user = build(:user, msn: 'foobar')
    assert_not user.valid?, 'Passed invalid MSN URL'
    assert_not_empty user.errors[:msn], 'No validation error for MSN present'
  end

  test 'should validate websites URL' do
    user = build(:user, websites: 'foobar')
    assert_not user.valid?, 'Passed invalid website URL'
    assert_not_empty user.errors[:websites], 'No validation error for website present'
  end

  # Methods
  test 'should reset flag count' do
    user = create(:user, :with_flag_count)
    user.reset_flag_count!
    assert_equal 0, user.flag_count, 'Flag count was not reset'
  end

  test 'should authenticate user' do
    user = create(:user, password: 'hunter2', password_confirmation: 'hunter2')
    assert_equal user, User.authenticate(user.email, 'hunter2'), "User wasn't authenticated"
  end

  test 'should encrypt user password' do
    user = build(:user)
    user.encrypt_password
    assert user.password_hash.present?, "Password wasn't encrypted"
  end

  test 'should setup user activation' do
    user = build(:user)
    user.setup_activation
    assert user.activate_token.present?, 'Did not set up user activation'
  end

  test 'should search user by terms and silo' do
    user = create(:user)
    search_results = User.search(user.name, user.university.silo)
    assert_includes search_results, user, "Couldn't find user by terms and silo"
  end

  test 'should get match with user' do
    user1 = create(:user)
    user2 = create(:user)
    match = create(:match, user_1: user2, user_2: user1)
    assert_equal match, user1.get_match_with(user2)
  end

  test 'should get random friend' do
    user1 = create(:user)
    user2 = create(:user)
    friendship = create(:friendship, user: user2, friend: user1, status: 'accepted')
    assert_not_empty user1.random_friend
  end

  test 'should get random match' do
    user1 = create(:user)
    user2 = create(:user)
    match = create(:match, user_1: user2, user_2: user1)
    assert_not_empty user1.random_match
  end

  test 'should get random inventory' do
    user = create(:user, :with_five_owned_items)
    assert_not_empty user.random_inventory
  end

  test 'should create user inventory after user creation' do
    user = create(:user)
    assert_not_nil user.inventory
  end

  test 'should add user to university after validation' do
    university = create(:university, domain: 'uni.edu')
    user = build(:user, email: 'john@uni.edu')
    user.valid?
    assert_equal university, user.university
  end
end
