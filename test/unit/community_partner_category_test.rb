require 'test_helper'

class CommunityPartnerCategoryTest < ActiveSupport::TestCase

  test 'should create valid CommunityPartnerCategory' do
    assert_difference 'CommunityPartnerCategory.count' do
      category = create(:community_partner_category)
    end
  end

  test 'should delete valid CommunityPartnerCategory' do
    category = create(:community_partner_category)
    assert_difference 'CommunityPartnerCategory.count', -1 do
      CommunityPartnerCategory.delete(category)
    end
  end

  test 'should not save CommunityPartnerCategory without name' do
    category = build(:community_partner_category, name: nil)
    assert_no_difference 'CommunityPartnerCategory.count', 'Saved CommunityPartnerCategory without name' do
      category.save
    end 
    assert_not_empty category.errors[:name], 'No validation error for name present'
  end
  
  test 'should have many CommunityPartners' do
    category = create(:community_partner_category_with_two_partners)
    assert_equal 2, category.community_partners.length
  end
end
