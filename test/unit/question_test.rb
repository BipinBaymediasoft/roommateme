require 'test_helper'

class QuestionTest < ActiveSupport::TestCase
  
  test 'should create valid Question' do
    assert_difference 'Question.count' do
      question = create(:question)
    end
  end

  test 'should delete valid Question' do
    question = create(:question)
    assert_difference 'Question.count', -1 do
      Question.delete(question)
    end
  end

  test 'should not save Question without content' do
    question = build(:question, content: nil)
    assert_no_difference 'Question.count', 'Saved Question without content' do
      question.save
    end 
    assert_not_empty question.errors[:content], 'No validation error for content present'
  end

  test 'should not save Question without form_id' do
    question = build(:question, form: nil)
    assert_no_difference 'Question.count', 'Saved Question without form_id' do
      question.save
    end 
    assert_not_empty question.errors[:form_id], 'No validation error for form_id present'
  end

  test 'should not save Question without weight' do
    question = build(:question, weight: nil)
    assert_no_difference 'Question.count', 'Saved Question without weight' do
      question.save
    end 
    assert_not_empty question.errors[:weight], 'No validation error for weight present'
  end

  test 'should have many answers' do
    question = create(:question_with_two_answers)
    assert_equal 2, question.answers.length
  end

  test 'should have many responses' do
    question = create(:question_with_responses)
    assert_equal 2, question.responses.length
  end

  test 'should return name' do
    question = create(:question)
    assert_equal "Question ##{question.id} F#{question.form.id}", question.name, "Returned a wrong question name"
  end

  test 'should collect users response' do
    user = create(:user)
    form = create(:form)
    question = create(:question, form: form)
    answer = create(:answer, question: question)
    response = create(:response, user: user, question: question, answer: answer, form: form)
    assert_equal response, question.collect_users_response(user, form)
  end

  test 'should return true if user has responses' do
    user = create(:user)
    form = create(:form)
    question = create(:question, form: form)
    answer = create(:answer, question: question)
    response = create(:response, user: user, form: form, question: question, answer: answer)
    assert question.user_has_responses(user, form)
  end

  test 'should return true if user has not responded' do
    user = create(:user)
    form = create(:form)
    question = create(:question, form: form)
    assert question.user_has_not_responded?(user, form)
  end

  test 'should return true if user has responded' do
    user = create(:user)
    form = create(:form)
    question = create(:question, form: form)
    answer = create(:answer, question: question)
    response = create(:response, user: user, form: form, question: question, answer: answer)
    assert question.user_has_responded?(user, form)
  end
end
