require 'test_helper'

class FriendshipTest < ActiveSupport::TestCase
  
  test 'should create valid Friendship' do
    assert_difference 'Friendship.count' do
      friendship = create(:friendship)
    end
  end

  test 'should delete valid Friendship' do
    friendship = create(:friendship)
    assert_difference 'Friendship.count', -1 do
      Friendship.delete(friendship)
    end
  end

  test 'should not save Friendship without user_id' do
    friendship = build(:friendship, user: nil)
    assert_no_difference 'Friendship.count', 'Saved Friendship without user_id' do
      friendship.save
    end 
    assert_not_empty friendship.errors[:user_id], 'No validation error for user_id present'
  end

  test 'should not save Friendship without friend_id' do
    friendship = build(:friendship, friend: nil)
    assert_no_difference 'Friendship.count', 'Saved Friendship without friend_id' do
      friendship.save
    end 
    assert_not_empty friendship.errors[:friend_id], 'No validation error for friend_id present'
  end

end
