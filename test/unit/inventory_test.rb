require 'test_helper'

class InventoryTest < ActiveSupport::TestCase
  
  # User has one Inventory which is created upon User save
  test 'should create valid Inventory' do
    assert_difference 'Inventory.count' do
      user = create(:user)
    end
  end

  test 'should delete valid Inventory' do
    user = create(:user)
    assert_difference 'Inventory.count', -1 do
      Inventory.delete(user.inventory)
    end
  end

  test 'should not save Inventory without user_id' do
    inventory = build(:inventory, user_id: nil)
    assert_no_difference 'Inventory.count', 'Saved Inventory without user_id' do
      inventory.save
    end 
    assert_not_empty inventory.errors[:user_id], 'No validation error for user_id present'
  end

end
