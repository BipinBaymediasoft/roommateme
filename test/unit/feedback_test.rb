require 'test_helper'

class FeedbackTest < ActiveSupport::TestCase
  
  test 'should create valid Feedback' do
    assert_difference 'Feedback.count' do
      feedback = create(:feedback)
    end
  end

  test 'should delete valid Feedback' do
    feedback = create(:feedback)
    assert_difference 'Feedback.count', -1 do
      Feedback.delete(feedback)
    end
  end

  test 'should not save Feedback without content' do
    feedback = build(:feedback, content: nil)
    assert_no_difference 'Feedback.count', 'Saved Feedback without name' do
      feedback.save
    end 
    assert_not_empty feedback.errors[:content], 'No validation error for content present'
  end

  test 'should return user name' do
    anonymous_feedback = create(:feedback)
    user_feedback = create(:feedback_from_user)
    assert_equal user_feedback.user.name, user_feedback.user_name
    assert_equal "Anonymous", anonymous_feedback.user_name
  end
  
  test 'should return category name' do
    feedback_without_category = create(:feedback)
    feedback_with_category = create(:feedback_with_category)
    assert_equal feedback_with_category.feedback_category.name, feedback_with_category.category_name
    assert_equal "No Category", feedback_without_category.category_name
  end
end
