require 'test_helper'

class InventoryItemTest < ActiveSupport::TestCase
  
  test 'should create valid InventoryItem' do
    assert_difference 'InventoryItem.count' do
      inventory_item = create(:inventory_item)
    end
  end

  test 'should delete valid InventoryItem' do
    inventory_item = create(:inventory_item)
    assert_difference 'InventoryItem.count', -1 do
      InventoryItem.delete(inventory_item)
    end
  end

  test 'should not save InventoryItem without inventory_id' do
    inventory_item = build(:inventory_item, inventory_id: nil)
    assert_no_difference 'InventoryItem.count', 'Saved InventoryItem without inventory_id' do
      inventory_item.save
    end 
    assert_not_empty inventory_item.errors[:inventory_id], 'No validation error for inventory_id present'
  end

  test 'should not save InventoryItem without item_id' do
    inventory_item = build(:inventory_item, item_id: nil)
    assert_no_difference 'InventoryItem.count', 'Saved InventoryItem without item_id' do
      inventory_item.save
    end 
    assert_not_empty inventory_item.errors[:item_id], 'No validation error for item_id present'
  end

  test 'should not save InventoryItem without user_id' do
    inventory_item = build(:inventory_item, user_id: nil)
    assert_no_difference 'InventoryItem.count', 'Saved InventoryItem without user_id' do
      inventory_item.save
    end 
    assert_not_empty inventory_item.errors[:user_id], 'No validation error for user_id present'
  end
end
