require 'test_helper'

class ItemCategoryTest < ActiveSupport::TestCase
  
  test 'should create valid ItemCategory' do
    assert_difference 'ItemCategory.count' do
      category = create(:item_category)
    end
  end

  test 'should delete valid ItemCategory' do
    category = create(:item_category)
    assert_difference 'ItemCategory.count', -1 do
      ItemCategory.delete(category)
    end
  end

  test 'should not save ItemCategory without name' do
    category = build(:item_category, name: nil)
    assert_no_difference 'ItemCategory.count', 'Saved ItemCategory without name' do
      category.save
    end 
    assert_not_empty category.errors[:name], 'No validation error for name present'
  end

  test 'should have many Items' do
    category = create(:item_category_with_two_items)
    assert_equal 2, category.items.length
  end
end
