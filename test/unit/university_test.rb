require 'test_helper'

class UniversityTest < ActiveSupport::TestCase

  test 'should create valid University' do
    assert_difference 'University.count' do
      university = create(:university)
    end
  end

  test 'should delete valid University' do
    university = create(:university)
    assert_difference 'University.count', -1 do
      University.delete(university)
    end
  end

  test 'should not save University without name' do
    university = build(:university, name: nil)
    assert_no_difference 'University.count', 'Saved University without name' do
      university.save
    end 
    assert_not_empty university.errors[:name], 'No validation error for name present'
  end

  test 'should not save University without acronym' do
    university = build(:university, acronym: nil)
    assert_no_difference 'University.count', 'Saved University without acronym' do
      university.save
    end 
    assert_not_empty university.errors[:acronym], 'No validation error for acronym present'
  end

  test 'should not save University without silo_id' do
    university = build(:university, silo: nil)
    assert_no_difference 'University.count', 'Saved University without silo_id' do
      university.save
    end 
    assert_not_empty university.errors[:silo_id], 'No validation error for silo_id present'
  end

  test 'should not save University without domain' do
    university = build(:university, domain: nil)
    assert_no_difference 'University.count', 'Saved University without domain' do
      university.save
    end 
    assert_not_empty university.errors[:domain], 'No validation error for domain present'
  end

  test 'should not save University without unique domain' do
    uni1 = create(:university)
    uni2 = build(:university, domain: uni1.domain)
    assert_no_difference 'University.count', 'Saved University without unique domain' do
      uni2.save
    end
  end

  # Associations
  test 'should have many users' do
    university = create(:university_with_users)
    assert_equal 2, university.users.length
  end
end
