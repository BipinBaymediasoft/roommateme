desc "Runs db: and ts: rebuilds."
task :rebuild do
  Rake::Task['ts:config'].invoke
  puts "\n\n\nRUNNING DB:REBUILD\n\n"
  Rake::Task['db:rebuild'].invoke
  puts "\n\n\nRUNNING TS:REBUILD\n\n"
  Rake::Task['ts:rebuild'].invoke
  puts "\n\n\nProject rebuilt."
end