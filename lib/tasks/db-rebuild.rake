namespace :db do
  desc "Runs :drop, :create, :migrate, and :seed."
  task :rebuild do
    puts "\n\nDropping database.\n\n"
    Rake::Task['db:drop'].invoke
    puts "\n\nCreating database.\n\n"
    Rake::Task['db:create'].invoke
    puts "\n\nMigrating database.\n\n"
    Rake::Task['db:migrate'].invoke
    puts "\n\nSeeding database.\n\n"
    Rake::Task['db:seed'].invoke
    puts "Database rebuilt."
  end
end