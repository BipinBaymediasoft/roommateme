# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20200222063553) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.integer  "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_admin_notes_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "name"
    t.integer  "silo_id"
    t.boolean  "is_super_admin"
    t.string   "email",                              default: "", null: false
    t.string   "encrypted_password",     limit: 128, default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "password_salt"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.integer  "failed_attempts",                    default: 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "authentication_token"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["authentication_token"], name: "index_admin_users_on_authentication_token", unique: true, using: :btree
  add_index "admin_users", ["confirmation_token"], name: "index_admin_users_on_confirmation_token", unique: true, using: :btree
  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree
  add_index "admin_users", ["unlock_token"], name: "index_admin_users_on_unlock_token", unique: true, using: :btree

  create_table "affiliate_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "affiliates", force: :cascade do |t|
    t.string   "name"
    t.string   "image"
    t.string   "description"
    t.integer  "affiliate_category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "delta",                 default: true, null: false
    t.text     "html_link"
  end

  create_table "answers", force: :cascade do |t|
    t.text     "content"
    t.integer  "question_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "form_id"
    t.integer  "points"
  end

  create_table "community_partner_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "community_partner_suggestions", force: :cascade do |t|
    t.string   "name",                null: false
    t.string   "phone",               null: false
    t.string   "email",               null: false
    t.string   "address",             null: false
    t.integer  "silo_id",             null: false
    t.string   "patronage_frequency", null: false
    t.text     "reason",              null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "community_partners", force: :cascade do |t|
    t.string   "name"
    t.string   "image"
    t.text     "description"
    t.integer  "silo_id"
    t.integer  "community_partner_category_id"
    t.text     "address"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "delta",                         default: true, null: false
    t.text     "google_maps"
  end

  create_table "feedback_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "internal",   default: false
  end

  create_table "feedbacks", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "feedback_category_id"
    t.string   "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "forms", force: :cascade do |t|
    t.string   "title"
    t.string   "instructions"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "friendships", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "friend_id"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "inventories", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "inventory_items", force: :cascade do |t|
    t.integer  "inventory_id"
    t.integer  "item_id"
    t.boolean  "have"
    t.boolean  "want"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
  end

  create_table "item_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "items", force: :cascade do |t|
    t.integer  "affiliate_id"
    t.text     "description"
    t.string   "image"
    t.integer  "item_category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.text     "html_link"
  end

  create_table "matches", force: :cascade do |t|
    t.integer  "user_1_id"
    t.integer  "user_2_id"
    t.integer  "percent"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "message_threads", force: :cascade do |t|
    t.string   "subject"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_read",    default: false
  end

  create_table "messages", force: :cascade do |t|
    t.text     "body"
    t.integer  "message_thread_id"
    t.integer  "author_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "participancies", force: :cascade do |t|
    t.integer  "thread_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "questions", force: :cascade do |t|
    t.text     "content"
    t.integer  "form_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "weight"
  end

  create_table "responses", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "form_id"
    t.integer  "question_id"
    t.integer  "answer_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "survey_response_id"
  end

  create_table "silos", force: :cascade do |t|
    t.string   "city"
    t.string   "state"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
  end

  create_table "survey_responses", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "form_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tutor_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tutors", force: :cascade do |t|
    t.string   "avatar"
    t.string   "name"
    t.string   "subjects"
    t.string   "contact"
    t.integer  "silo_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "tutor_category_id"
  end

  create_table "universities", force: :cascade do |t|
    t.integer  "silo_id"
    t.string   "name"
    t.string   "acronym"
    t.string   "logo"
    t.text     "address"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "google_maps"
    t.string   "domain"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email"
    t.string   "password_hash"
    t.string   "password_salt"
    t.boolean  "activated"
    t.string   "activate_token"
    t.string   "name"
    t.string   "avatar"
    t.string   "sex"
    t.text     "websites"
    t.text     "about_me"
    t.integer  "flag_count"
    t.integer  "university_id"
    t.integer  "survey_id"
    t.boolean  "is_in_college"
    t.boolean  "payed_for",        default: true
    t.boolean  "needs_roommate"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "delta",            default: true, null: false
    t.text     "perfect_roommate"
    t.string   "major"
    t.string   "ethnicity"
    t.string   "languages"
    t.string   "religion"
    t.text     "music"
    t.text     "activities"
    t.text     "shows"
    t.string   "birthtown"
    t.string   "phone"
    t.string   "aim"
    t.string   "yahoo"
    t.string   "msn"
    t.string   "facebook"
    t.string   "myspace"
    t.string   "recovery"
    t.date     "dob"
    t.boolean  "willing_to_move",  default: true
    t.string   "zipcode"
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.float    "latitude"
    t.float    "longitude"
  end

end
