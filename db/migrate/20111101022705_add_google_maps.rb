class AddGoogleMaps < ActiveRecord::Migration
  def self.up
    add_column :community_partners, :google_maps, :text
    add_column :universities, :google_maps, :text
  end

  def self.down
    remove_column :universities, :google_maps
    remove_column :community_partners, :google_maps
  end
end