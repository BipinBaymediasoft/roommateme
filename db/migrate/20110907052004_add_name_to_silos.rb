class AddNameToSilos < ActiveRecord::Migration
  def self.up
    add_column :silos, :name, :string
  end

  def self.down
    remove_column :silos, :name
  end
end
