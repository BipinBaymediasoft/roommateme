class CreateSurveyResponses < ActiveRecord::Migration
  def self.up
    create_table :survey_responses do |t|
      t.belongs_to :user
      t.belongs_to :form

      t.timestamps
    end
  end

  def self.down
    drop_table :survey_responses
  end
end
