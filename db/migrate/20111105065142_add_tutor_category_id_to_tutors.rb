class AddTutorCategoryIdToTutors < ActiveRecord::Migration
  def self.up
    add_column :tutors, :tutor_category_id, :integer
  end

  def self.down
    remove_column :tutors, :tutor_category_id
  end
end
