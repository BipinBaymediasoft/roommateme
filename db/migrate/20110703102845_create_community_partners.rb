class CreateCommunityPartners < ActiveRecord::Migration
  def self.up
    create_table :community_partners do |t|
      t.string :name
      t.string :image
      t.string :description
      t.integer :silo_id
      t.integer :community_partner_category_id
      t.text :address

      t.timestamps
    end
  end

  def self.down
    drop_table :community_partners
  end
end
