class CreateItems < ActiveRecord::Migration
  def self.up
    create_table :items do |t|
      t.integer :affiliate_id
      t.text :description
      t.string :image
      t.integer :item_category_id

      t.timestamps
    end
  end

  def self.down
    drop_table :items
  end
end
