class CreateTutors < ActiveRecord::Migration
  def self.up
    create_table :tutors do |t|
      t.string :avatar
      t.string :name
      t.string :subjects
      t.string :contact
      t.integer :silo_id

      t.timestamps
    end
  end

  def self.down
    drop_table :tutors
  end
end
