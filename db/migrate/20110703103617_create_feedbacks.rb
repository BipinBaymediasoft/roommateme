class CreateFeedbacks < ActiveRecord::Migration
  def self.up
    create_table :feedbacks do |t|
      t.integer :user_id
      t.integer :feedback_category_id
      t.string :content

      t.timestamps
    end
  end

  def self.down
    drop_table :feedbacks
  end
end
