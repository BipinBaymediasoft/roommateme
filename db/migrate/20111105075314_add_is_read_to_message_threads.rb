class AddIsReadToMessageThreads < ActiveRecord::Migration
  def self.up
    add_column :message_threads, :is_read, :boolean, :default => false
  end

  def self.down
    remove_column :message_threads, :is_read
  end
end
