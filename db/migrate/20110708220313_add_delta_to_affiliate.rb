class AddDeltaToAffiliate < ActiveRecord::Migration
  def self.up
    add_column :affiliates, :delta, :boolean, :default => true, :null => false
  end

  def self.down
    remove_column :affiliates, :delta
  end
end
