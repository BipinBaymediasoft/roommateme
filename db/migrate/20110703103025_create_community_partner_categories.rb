class CreateCommunityPartnerCategories < ActiveRecord::Migration
  def self.up
    create_table :community_partner_categories do |t|
      t.string :name

      t.timestamps
    end
  end

  def self.down
    drop_table :community_partner_categories
  end
end
