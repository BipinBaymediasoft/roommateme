class AddDobToUsers < ActiveRecord::Migration
  def self.up
    remove_column :users, :dob
    add_column :users, :dob, :date
  end

  def self.down
    add_column :users, :dob, :string
    remove_column :users, :dob
  end
end
