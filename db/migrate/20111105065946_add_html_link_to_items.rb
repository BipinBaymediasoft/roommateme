class AddHtmlLinkToItems < ActiveRecord::Migration
  def self.up
    add_column :items, :html_link, :text
  end

  def self.down
    remove_column :items, :html_link
  end
end
