class AddRecoveryToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :recovery, :string
  end

  def self.down
    remove_column :users, :recovery
  end
end
