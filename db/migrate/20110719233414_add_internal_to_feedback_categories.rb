class AddInternalToFeedbackCategories < ActiveRecord::Migration
  def self.up
    add_column :feedback_categories, :internal, :boolean, :default => false
  end

  def self.down
    remove_column :feedback_categories, :internal
  end
end
