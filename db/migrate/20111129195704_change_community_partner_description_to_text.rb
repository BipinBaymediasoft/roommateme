class ChangeCommunityPartnerDescriptionToText < ActiveRecord::Migration
  def self.up
    change_column :community_partners, :description, :text
  end

  def self.down
    change_column :community_partners, :description, :string
  end
end