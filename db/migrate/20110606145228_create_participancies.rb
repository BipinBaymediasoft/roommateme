class CreateParticipancies < ActiveRecord::Migration
  def self.up
    create_table :participancies do |t|
      t.integer :thread_id
      t.integer :user_id

      t.timestamps
    end
  end

  def self.down
    drop_table :participancies
  end
end
