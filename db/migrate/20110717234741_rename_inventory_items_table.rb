class RenameInventoryItemsTable < ActiveRecord::Migration
  def self.up
    rename_table :inventoryitems, :inventory_items
  end

  def self.down
    rename_table :inventory_items, :inventoryitems
  end
end
