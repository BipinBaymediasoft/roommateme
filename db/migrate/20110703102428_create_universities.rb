class CreateUniversities < ActiveRecord::Migration
  def self.up
    create_table :universities do |t|
      t.integer :silo_id
      t.string :name
      t.string :acronym
      t.string :logo
      t.text :address

      t.timestamps
    end
  end

  def self.down
    drop_table :universities
  end
end
