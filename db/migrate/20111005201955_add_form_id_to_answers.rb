class AddFormIdToAnswers < ActiveRecord::Migration
  def self.up
    add_column :answers, :form_id, :integer
  end

  def self.down
    remove_column :answers, :form_id
  end
end
