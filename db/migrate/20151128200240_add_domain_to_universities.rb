class AddDomainToUniversities < ActiveRecord::Migration
  def self.up
    add_column :universities, :domain, :string
    University.all.each do |u|
      u.update_attributes :domain => u.acronym.downcase + ".edu"
    end
  end

  def self.down
    remove_column :universities, :domain
  end
end
