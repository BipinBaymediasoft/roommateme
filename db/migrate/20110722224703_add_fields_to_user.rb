class AddFieldsToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :perfect_roommate, :text
    add_column :users, :major, :string
    add_column :users, :ethnicity, :string
    add_column :users, :languages, :string
    add_column :users, :religion, :string
    add_column :users, :music, :text
    add_column :users, :activities, :text
    add_column :users, :shows, :text
    add_column :users, :birthtown, :string
    add_column :users, :phone, :string
    add_column :users, :aim, :string
    add_column :users, :yahoo, :string
    add_column :users, :msn, :string
    add_column :users, :facebook, :string
    add_column :users, :myspace, :string
  end

  def self.down
    remove_column :users, :perfect_roommate
    remove_column :users, :major
    remove_column :users, :ethnicity
    remove_column :users, :languages
    remove_column :users, :religion
    remove_column :users, :music
    remove_column :users, :activities
    remove_column :users, :shows
    remove_column :users, :birthtown
    remove_column :users, :phone
    remove_column :users, :aim
    remove_column :users, :yahoo
    remove_column :users, :msn
    remove_column :users, :facebook
    remove_column :users, :myspace
  end
end
