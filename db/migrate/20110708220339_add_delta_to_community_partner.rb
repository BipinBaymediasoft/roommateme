class AddDeltaToCommunityPartner < ActiveRecord::Migration
  def self.up
    add_column :community_partners, :delta, :boolean, :default => true, :null => false
  end

  def self.down
    remove_column :community_partners, :delta
  end
end
