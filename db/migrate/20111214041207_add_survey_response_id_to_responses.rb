class AddSurveyResponseIdToResponses < ActiveRecord::Migration
  def self.up
    add_column :responses, :survey_response_id, :integer
  end

  def self.down
    remove_column :responses, :survey_response_id
  end
end
