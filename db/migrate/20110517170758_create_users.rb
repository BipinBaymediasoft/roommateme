class CreateUsers < ActiveRecord::Migration
  def self.up
    create_table :users do |t|
      t.string :email
      t.string :password_hash
      t.string :password_salt
      t.boolean :activated
      t.string :activate_token
      
      t.string :name
      t.string :avatar
      t.string :sex
      t.string :dob
      t.text :websites
      t.text :about_me
      
      t.integer :flag_count
      t.integer :university_id
      t.integer :survey_id
      t.boolean :is_in_college
      t.boolean :payed_for, :default => true
      t.boolean :needs_roommate

      t.timestamps
    end
  end

  def self.down
    drop_table :users
  end
end
