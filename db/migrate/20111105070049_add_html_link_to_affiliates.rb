class AddHtmlLinkToAffiliates < ActiveRecord::Migration
  def self.up
    add_column :affiliates, :html_link, :text
  end

  def self.down
    remove_column :affiliates, :html_link
  end
end
