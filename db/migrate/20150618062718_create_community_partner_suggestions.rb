class CreateCommunityPartnerSuggestions < ActiveRecord::Migration
  def self.up
    create_table :community_partner_suggestions do |t|
      t.string :name, :null => false
      t.string :phone, :null => false
      t.string :email, :null => false
      t.string :address, :null => false
      t.references :silo, :null => false
      t.string :patronage_frequency, :null => false
      t.text :reason, :null => false

      t.timestamps
    end
  end

  def self.down
    drop_table :community_partner_suggestions
  end
end
