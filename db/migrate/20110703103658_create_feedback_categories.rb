class CreateFeedbackCategories < ActiveRecord::Migration
  def self.up
    create_table :feedback_categories do |t|
      t.string :name

      t.timestamps
    end
  end

  def self.down
    drop_table :feedback_categories
  end
end
