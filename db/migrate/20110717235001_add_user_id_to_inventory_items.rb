class AddUserIdToInventoryItems < ActiveRecord::Migration
  def self.up
    add_column :inventory_items, :user_id, :integer
  end

  def self.down
    remove_column :inventory_items, :user_id
  end
end
