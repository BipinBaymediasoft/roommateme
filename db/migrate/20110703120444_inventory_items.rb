class InventoryItems < ActiveRecord::Migration
  def self.up
    create_table :inventoryitems do |t|
      t.integer :inventory_id
      t.integer :item_id
      t.boolean :have
      t.boolean :want

      t.timestamps
    end
  end

  def self.down
    drop_table :inventoryitems
  end
end