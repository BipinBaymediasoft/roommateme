class CreateMatches < ActiveRecord::Migration
  def self.up
    create_table :matches do |t|
      t.integer :user_1_id
      t.integer :user_2_id
      t.integer :percent

      t.timestamps
    end
  end

  def self.down
    drop_table :matches
  end
end
