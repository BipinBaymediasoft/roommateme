class CreateTutorCategories < ActiveRecord::Migration
  def self.up
    create_table :tutor_categories do |t|
      t.string :name

      t.timestamps
    end
  end

  def self.down
    drop_table :tutor_categories
  end
end
