# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

master_and_commander = AdminUser.create!(:silo_id => 1, :name => "RoommateMe Administrator", :email => 'admin@roommateme.net', :password => 'password', :is_super_admin => true)
master_and_commander.save

Silo.create([ {:city => 'Baton Rouge', :state => 'LA'},
              {:city => 'New Orleans', :state => 'LA'},
              {:city => 'Dallas', :state => 'TX'},
              {:city => 'El Dorado', :state => 'NA'}
            ])

University.create([ {:name => 'Louisiana State University', :acronym => 'lsu', :domain => 'lsu.edu', :silo_id => 1},
                    {:name => 'Baton Rouge Community College', :acronym => 'brcc', :domain => 'brcc.edu', :silo_id => 1},
                    {:name => 'Tulane University', :acronym => 'tulane', :domain => 'tulane.edu', :silo_id => 2},
                    {:name => 'RoommateMe Organization', :acronym => 'RMM', :domain => 'roommateme.net', :silo_id => 2},
                    {:name => 'Fakedy Fake fake fake', :acronym => 'fake', :domain => 'fake.edu', :silo_id => 3}
                  ])

awesome_users = [ 
                  User.create({:name => 'Evan Cordell', :activated => true, :password => 'evan', :email => 'evan@fake.edu', :about_me => 'Evan is an immortal rainbow panda.', :sex => "Male", :dob => "0/0/2001", :flag_count => 0 }),
                  User.create({:name => 'Samwise Gamgee', :activated => true, :password => 'word', :email => 'word@fake.edu', :about_me => 'Evan is an immortal rainbow panda.', :sex => "Male", :dob => "0/0/2001", :flag_count => 0 }),
                  User.create({:name => 'Felix the Cat', :activated => true, :password => 'word', :email => 'word1@fake.edu', :about_me => 'Evan is an immortal rainbow panda.', :sex => "Male", :dob => "0/0/2001", :flag_count => 5 }),
                  User.create({:name => 'Tyger Tyger', :activated => true, :password => 'word', :email => 'word2@fake.edu', :about_me => 'Evan is an immortal rainbow panda.', :sex => "Male", :dob => "0/0/2001", :flag_count => 0 }),
                  User.create({:name => 'Das Boot', :activated => true, :password => 'word', :email => 'word3@fake.edu', :about_me => 'Evan is an immortal rainbow panda.', :sex => "Male", :dob => "0/0/2001", :flag_count => 0 }),
                  User.create({:name => 'Cat Stevens', :activated => true, :password => 'word', :email => 'word4@fake.edu', :about_me => 'Evan is an immortal rainbow panda.', :sex => "Male", :dob => "0/0/2001", :flag_count => 1 }),
                  User.create({:name => 'Lime Light', :activated => true, :password => 'word', :email => 'word5@fake.edu', :about_me => 'Evan is an immortal rainbow panda.', :sex => "Male", :dob => "0/0/2001", :flag_count => 0 })
                ]
awesome_users.each do |u|
  
  u.survey_id=1
  u.encrypt_password
  u.activated=true
  
  email, address = Hash.new(), Hash.new()
  email[:address]=u.email.split("@")[1]
  address=email[:address].split(".")
  domain=address[address.length-2]
  university=University.find_by_acronym(domain)
  u.university_id=university.object_id
  
  u.save
  Inventory.create({:user_id=>u.id})
end

friend1 = {:user_id => 1, :friend_id => 2, :status => 'accepted'}
friend2 = {:user_id => 2, :friend_id => 1, :status => 'accepted'}
Friendship.create(friend1)
Friendship.create(friend2)
friend1 = {:user_id => 1, :friend_id => 3, :status => 'accepted'}
friend2 = {:user_id => 3, :friend_id => 1, :status => 'accepted'}
Friendship.create(friend1)
Friendship.create(friend2)
friend1 = {:user_id => 4, :friend_id => 2, :status => 'accepted'}
friend2 = {:user_id => 2, :friend_id => 4, :status => 'accepted'}
Friendship.create(friend1)
Friendship.create(friend2)
friend1 = {:user_id => 5, :friend_id => 2, :status => 'accepted'}
friend2 = {:user_id => 2, :friend_id => 5, :status => 'accepted'}
Friendship.create(friend1)
Friendship.create(friend2)
friend1 = {:user_id => 3, :friend_id => 6, :status => 'accepted'}
friend2 = {:user_id => 6, :friend_id => 3, :status => 'accepted'}
Friendship.create(friend1)
Friendship.create(friend2)
friend1 = {:user_id => 3, :friend_id => 7, :status => 'accepted'}
friend2 = {:user_id => 7, :friend_id => 3, :status => 'accepted'}
Friendship.create(friend1)
Friendship.create(friend2)
friend1 = {:user_id => 1, :friend_id => 8, :status => 'accepted'}
friend2 = {:user_id => 8, :friend_id => 1, :status => 'accepted'}
Friendship.create(friend1)
Friendship.create(friend2)
friend1 = {:user_id => 1, :friend_id => 9, :status => 'accepted'}
friend2 = {:user_id => 9, :friend_id => 1, :status => 'accepted'}
Friendship.create(friend1)
Friendship.create(friend2)
                  
CommunityPartnerCategory.create([ {:name => 'Dining'}, {:name => 'Clothing'}, {:name => 'Sports'} ])
CommunityPartner.create([ {:name => 'East Chimes', :community_partner_category_id => 1, :silo_id => 1},
                          {:name => 'The Boot', :community_partner_category_id => 1, :silo_id => 2 },
                          {:name => 'Storyville', :community_partner_category_id => 2, :silo_id => 1},
                          {:name => 'Red Stick Sports', :community_partner_category_id => 3, :silo_id => 1},
                          {:name => 'Brightside Bar and Grill', :community_partner_category_id => 1, :silo_id => 1},
                          {:name => 'Chinese Combo King', :community_partner_category_id => 1, :silo_id => 1},
                          {:name => 'Louie\'s', :community_partner_category_id => 1, :silo_id => 1},
                          {:name => 'Madeline\'s', :community_partner_category_id => 1, :silo_id => 1}
                        ])

AffiliateCategory.create([ {:name => 'Books'}, {:name => 'Furniture'}, {:name => 'Posters'} ])
Affiliate.create([  {:name => "Ikea", :affiliate_category_id => 2, :description => "Description"},
                    {:name => "Chimes Textbook Exchange", :affiliate_category_id => 1, :description => "Description"},
                    {:name => "Co-op", :affiliate_category_id => 1, :description => "Description"},
                    {:name => "LSU Union Poster Company", :affiliate_category_id => 3, :description => "Description"},
                    {:name => "Made-up Tulane Furniture", :affiliate_category_id => 2, :description => "Description"},
                    {:name => "Made-up Loyola Furniture", :affiliate_category_id => 2, :description => "Description"},
                    {:name => "Made-up Tulane Books", :affiliate_category_id => 1, :description => "Description"}
                ])

ItemCategory.create([ {:name => 'Beds'}, {:name => 'Desks'}, {:name => 'Tables'}, {:name => 'Seating'},
                      {:name => 'Cleaning'}, {:name => 'Rugs'}, {:name => 'Lighting'}, {:name => 'Electronics'},
                      {:name => 'Outdoors'}, {:name => 'Other'} ])
Item.create([ {:name => "SuperSoftChair2000", :description => 'A soft chair.', :item_category_id => 1, :affiliate_id => 1}, 
              {:name => "SuperHardChair2000", :description => 'A hard chair.', :item_category_id => 1, :affiliate_id => 1},
              {:name => "SuperSoftCouch2000", :description => 'A soft couch.', :item_category_id => 2, :affiliate_id => 1},
              {:name => "SuperHardCouch2000", :description => 'A hard couch.', :item_category_id => 2, :affiliate_id => 1},
              {:name => "SuperSoftBed2000", :description => 'A soft bed.', :item_category_id => 3, :affiliate_id => 1},
              {:name => "SuperHardBed2000", :description => 'A hard bed.', :item_category_id => 3, :affiliate_id => 1},
              {:name => "SuperSoftRug2000", :description => 'A soft rug.', :item_category_id => 4, :affiliate_id => 1},
              {:name => "SuperHardRug2000", :description => 'A hard rug.', :item_category_id => 4, :affiliate_id => 1}
            ])

FeedbackCategory.create([ {:name => 'Suggestion'}, {:name => 'Complaint'}, {:name => 'Bug'}, {:name => 'Flag Report', :internal => true} ])
Feedback.create([ {:user_id => 1, :feedback_category_id => 1, :content => "I love this site!"},
                  {:user_id => 2, :feedback_category_id => 1, :content => "This site changed my life!"},
                  {:user_id => 3, :feedback_category_id => 2, :content => "This site gave me a mole."},
                  {:user_id => 4, :feedback_category_id => 3, :content => "Sometimes I can't see this website's reflection in a mirror."},
                  {:user_id => 5, :feedback_category_id => 4, :content => "Christopher Keele from LSU was being boisterous and rude in a private message to me."},
                  {:user_id => 7, :feedback_category_id => 4, :content => "Christopher Keele from LSU had a vulgar picture in his profile."}
                ])
                
Tutor.create([ {:name => "Logan Leger", :subjects => "Love, Calculus 1", :contact => "explosive", :silo_id => 1}  
            ])

# MessageThread.create([ {} ])
# Participancy.create([ {} ])
# Message.create([ {} ])
# 
# Match.create(:user_1_id => "1", :user_2_id => "2", :percent => "34")
# Match.create(:user_1_id => "2", :user_2_id => "1", :percent => "34")
# 
# Match.create(:user_1_id => "3", :user_2_id => "2", :percent => "88")
# Match.create(:user_1_id => "2", :user_2_id => "3", :percent => "88")
# 
# Match.create(:user_1_id => "1", :user_2_id => "5", :percent => "66")
# Match.create(:user_1_id => "5", :user_2_id => "1", :percent => "66")
# 
# Match.create(:user_1_id => "1", :user_2_id => "7", :percent => "93")
# Match.create(:user_1_id => "7", :user_2_id => "1", :percent => "93")
# 
# Match.create(:user_1_id => "1", :user_2_id => "8", :percent => "12")
# Match.create(:user_1_id => "8", :user_2_id => "1", :percent => "12")

form = Form.create(:title => "RoommateMe Survey", :instructions => "This is the first version of our powerful roommate matching survey. Respond to each question with greatest accuracy.")

questions = Question.create([ 
	{:content => "How often do you smoke?", :form_id => form.id, :weight => 4},
	{:content => "On average, how often do you clean your bedroom?", :form_id => form.id, :weight => 4},
	{:content => "On average, how often do you clean your bathroom?", :form_id => form.id, :weight => 4},
	{:content => "On average, how often do you clean your living room?", :form_id => form.id, :weight => 4},
	{:content => "I believe that someone should always leave a living area in the same condition as they found it in (if it.s clean or dirty before you get there, it should remain clean or dirty after):", :form_id => form.id, :weight => 4},
	{:content => "Your policy on dishes:", :form_id => form.id, :weight => 4},
	{:content => "Trash should be taken out:", :form_id => form.id, :weight => 4},
	{:content => "I like to eat out rather than cook at home:", :form_id => form.id, :weight => 4},
	{:content => "I usually sleep:", :form_id => form.id, :weight => 4},
	{:content => "I usually wake up:", :form_id => form.id, :weight => 4},
	{:content => "I usually go to bed:", :form_id => form.id, :weight => 4},
	{:content => "I am a light sleeper:", :form_id => form.id, :weight => 4},
	{:content => "I would prefer if all household chores and cleaning tasks were structured and distributed equally among roommates in a schedule:", :form_id => form.id, :weight => 4},
	{:content => "I would prefer to share groceries and household supplies: ", :form_id => form.id, :weight => 4},
	{:content => "It bothers me when someone does not clean up after themselves:", :form_id => form.id, :weight => 4},
	{:content => "When I sleep, I prefer the indoor temperature to be:", :form_id => form.id, :weight => 4},
	{:content => "I prefer to use less AC when it is hot outside and less heat when it is cold outside to conserve energy and save money:", :form_id => form.id, :weight => 4},
	{:content => "On average, how often do you consume alcohol?", :form_id => form.id, :weight => 4},
	{:content => "I prefer the interior decoration style:", :form_id => form.id, :weight => 4},
	{:content => "I would prefer to equally divide cooking responsibilities for each roommate:", :form_id => form.id, :weight => 4},
	{:content => "On average, how often does your significant other/boyfriend/girlfriend stay overnight? ", :form_id => form.id, :weight => 4},
	{:content => "On average, how often do you have guests come to visit?", :form_id => form.id, :weight => 4},
	{:content => "I do my laundry:", :form_id => form.id, :weight => 4},
	{:content => "I am very cleanly:", :form_id => form.id, :weight => 4},
	{:content => "I am very organized:", :form_id => form.id, :weight => 4},
	{:content => "I am very neat and tidy:", :form_id => form.id, :weight => 4},
	{:content => "If the only available washing machine is being used my roommates and I need to wash my clothes:", :form_id => form.id, :weight => 4},
	{:content => "I spend most of my free time at home:", :form_id => form.id, :weight => 3},
	{:content => "When I.m at home I like to spend most of my time in my room:", :form_id => form.id, :weight => 3},
	{:content => "How would you most likely react in social situations where you don.t know anyone?", :form_id => form.id, :weight => 3},
	{:content => "I spend most of my free time with other people versus doing solo activities.", :form_id => form.id, :weight => 3},
	{:content => "If someone does something that offends me, I prefer to handle the situation by:", :form_id => form.id, :weight => 3},
	{:content => "Religion is important to me.", :form_id => form.id, :weight => 3},
	{:content => "I enjoy talking or hanging out with people of different cultures.", :form_id => form.id, :weight => 3},
	{:content => "I enjoy talking or hanging out with people of different ethnicities.", :form_id => form.id, :weight => 3},
	{:content => "I enjoy talking to or hanging out with people of different races:", :form_id => form.id, :weight => 3},
	{:content => "I enjoy talking to or hanging out with people of different ages:", :form_id => form.id, :weight => 3},
	{:content => "I enjoy talking to or hanging out with people of different sexual orientations:", :form_id => form.id, :weight => 3},
	{:content => "I would pass up the best party/event of the year to study for a very important test:", :form_id => form.id, :weight => 3},
	{:content => "If I found a $100 dollar bill in the kitchen of my apartment, I would:", :form_id => form.id, :weight => 3},
	{:content => "I believe that most all good things in life require hard work:", :form_id => form.id, :weight => 3},
	{:content => "I consider myself health conscious with regard to my eating habits:", :form_id => form.id, :weight => 3},
	{:content => "I have a strict workout schedule and do physical activity regularly:", :form_id => form.id, :weight => 3},
	{:content => "I consider reading one of my favorite pastimes. ", :form_id => form.id, :weight => 3},
	{:content => "I consider watching TV as one of my favorite pastimes.", :form_id => form.id, :weight => 3},
	{:content => "I consider watching movies to be one of my favorite pastimes.", :form_id => form.id, :weight => 3},
	{:content => "I am a connoisseur of good music", :form_id => form.id, :weight => 3},
	{:content => "I enjoy playing card games", :form_id => form.id, :weight => 3},
	{:content => "Gambling is fun.", :form_id => form.id, :weight => 3},
	{:content => "What is your favorite kind of TV show?", :form_id => form.id, :weight => 2},
	{:content => "How much do you enjoy playing sports?", :form_id => form.id, :weight => 2},
	{:content => "How much do you enjoy watching sports on TV?", :form_id => form.id, :weight => 2},
	{:content => "How much do you enjoy outdoors activities (hiking, biking, surfing.)?", :form_id => form.id, :weight => 2},
	{:content => "How much do you enjoy going to parties?", :form_id => form.id, :weight => 2},
	{:content => "How much do you enjoy going to nightclubs/bars?", :form_id => form.id, :weight => 2},
	{:content => "How much do you enjoy playing music?", :form_id => form.id, :weight => 2},
	{:content => "How much do you enjoy going to concerts?", :form_id => form.id, :weight => 2},
	{:content => "I am a very giving person:", :form_id => form.id, :weight => 2},
	{:content => "I put others before myself:", :form_id => form.id, :weight => 2},
	{:content => "I can be confrontational:", :form_id => form.id, :weight => 1},
	{:content => "I can be very opinionated:", :form_id => form.id, :weight => 1},
	{:content => "I am open-minded:", :form_id => form.id, :weight => 1},
	{:content => "What is your favorite kind of music?", :form_id => form.id, :weight => 3},
	{:content => "How much do you enjoy creating visual artwork?", :form_id => form.id, :weight => 3},
	{:content => "How much do you enjoy viewing visual artwork?", :form_id => form.id, :weight => 3},
	{:content => "How much do you enjoy participating in performing arts?", :form_id => form.id, :weight => 3},
	{:content => "How much do you enjoy viewing performing arts?", :form_id => form.id, :weight => 3},
	{:content => "I think roommates should be able to have fun together:", :form_id => form.id, :weight => 3},
	{:content => "If there is a problem, roommates should be able to work it out together", :form_id => form.id, :weight => 3},
	{:content => "Roommates should help each other out when needed:", :form_id => form.id, :weight => 3},
	{:content => "Roommates should be able to confide in each other:", :form_id => form.id, :weight => 1},
	{:content => "Sometimes roommates will get in arguments together:", :form_id => form.id, :weight => 1},
	{:content => "If I do something that bothers someone I would try my best to make it up to them:", :form_id => form.id, :weight => 1},
	{:content => "If someone was bothering my roommate, I would stand up for them", :form_id => form.id, :weight => 1},
	{:content => "I enjoy the company of cats:", :form_id => form.id, :weight => 4},
	{:content => "I enjoy the company of dogs:", :form_id => form.id, :weight => 4},
	{:content => "I would like my roommates to help me with my pet care responsibilities:", :form_id => form.id, :weight => 4},
	{:content => "I would be willing to help with another roommate.s pet responsibilities:", :form_id => form.id, :weight => 4}
])

Question.all.each do |question|
  answers = Answer.create([ {:content => "Strongly Agree", :question_id => question.id, :form_id => form.id, :points => 1},
                            {:content => "Agree", :question_id => question.id, :form_id => form.id, :points => 2},
                            {:content => "Neutral", :question_id => question.id, :form_id => form.id, :points => 3},
                            {:content => "Disagree", :question_id => question.id, :form_id => form.id, :points => 4},
                            {:content => "Strongly Disagree", :question_id => question.id, :form_id => form.id, :points => 5}
                          ])
end
